-- копипаст (см Гатиатуллин)
module HW1
       ( h1
       , h2
       , f
       , isp
       , prs
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты
-- |Вычислить сумму двух аргументов
h1 :: Integer -> Integer -> Integer
h1 y x = x + y

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
h2 :: Integer -> Double
h2 n = sum $ map (\k -> 1.0 / fromIntegral (k ^ k)) [1,2..n]

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
f :: Integer -> Integer
f n = product (if odd n then odds n else evens n)
odds n = [x | x <- [1,3..n]]
evens n = [x | x <- [2,4..n]]

-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isp :: Integer -> Bool
isp p = p > 1 && all (\x -> mod p x /= 0) [2..floor(sqrt (fromIntegral p))]

-- |Найти сумму всех простых чисел в диапазоне [a;b]
prs :: Integer -> Integer -> Integer
prs x y = sum (filter isp [x..y])

