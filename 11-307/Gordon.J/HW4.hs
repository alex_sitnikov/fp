module HW4
   ( insertValue
   , slice
   , collectNodes
   , isEulerPathExist
   , paths
   ) where
import Data.List
-- |������� insertValue ������ ������ ��������� �������� � ������ �� ��������� 
-- �������. 
insertValue :: a -> [a] -> Int -> [a]
insertValue x  list n = take n list ++ [x] ++ drop n list


-- |������� slice ������ ���������� ��������� � �������� �� A �� B 
--		������	 �		B
slice :: [a] -> Int -> Int -> Maybe [a]
slice list a b= 
	if (length (take (b-a) (drop (a-1) list)) ==0) then Nothing
		else Just (take (b-a) (drop (a-1) list))

-- |�������� ������� collectNodes, ������� ��� ������� ������ ������ �������� ������
-- �������� ����� � ������� ����� ������ ���� �������.
-- �������� (��. �����������)
data Tree a = Node a [Tree a]

collectNodes :: Tree a -> [[a]]
collectNodes (Node v ts) = [v] : collectNodes' ts []

collectNodes' :: [Tree a] -> [[a]] -> [[a]]
collectNodes' [] res    = res
collectNodes' level res = res ++ [levelValues level] ++ collectNodes' (nodesBelow level) res

levelValues :: [Tree a] -> [a]
levelValues ts = map (\(Node n _) -> n) ts

nodesBelow :: [Tree a] -> [Tree a]
nodesBelow xs = flatmap treesOf xs



treesOf :: Tree a -> [Tree a]
treesOf (Node _ xs) = xs

flatmap _ [] = []
flatmap f (x:xs) = f x ++ flatmap f xs
-- |������� isEulerPathExist ������ ��������� �������� �� ���� ������� ���� 
--type Graph = [(Int, [Int])]

--isEulerPathExist :: Graph -> Bool
type Graph = [(Int, [Int])]
getVerticesCount:: Graph -> Int
getVerticesCount ((x,t):s)= 
	if mod (length t) 2 >0 then 1 + (getVerticesCount s)
	else (getVerticesCount s)
isEulerPathExist :: Graph -> Bool
isEulerPathExist a = 
	if getVerticesCount a > 2 then False
		else True

-- |������� paths ������ ������� ������ ����� ����� 2�� ������� � �����.
-- ������ ���� �������������� ����� ������ � ��������� �� ����� � �� B.
-- ���� �������� ��� ������ ����� [(a,a)]. ��������, [(1,2),(2,3),(1,3),(3,4),(4,2),(5,6)]
--				 �	  B		����		
paths :: Eq a => a -> a -> [(a,a)] -> [[a]]
paths a b g = map (\path -> a : path) (pathsInternal a b g [a])
  where
    pathsInternal a b g visited
        | a == b    = [[]]
        | otherwise = [c:path | c <- neighbors a g, c `notElem` visited, path <- pathsInternal c b g (c:visited)]


neighbors :: Eq a => a -> [(a,a)] -> [a]
neighbors a g = map snd filtered
  where
    filtered = filter (\edge -> fst edge == a) g