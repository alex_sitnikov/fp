-- просрочено (/2)
module HW3
       ( root
       , sequence2_3_f
       , symmetric
       , listDigits
       , isHeap
       ) where

import Data.List
import Data.Char

-- |Функция root должна вычислить приближенное значение корня уравнения
-- tan x = 1 - x с точностью, заданной первым (и единственным) аргументом
-- функции
f :: Double -> Double
f x = tan x + x - 1

root :: Double -> Double
root eps = error "root is not implemented"

-- |Бесконечная упорядоченная последовательность целых чисел без повторений
-- составлена из всех квадратов, кубов и факториалов натуральных чисел
-- ([1, 2, 4, 8, 9, 16, 24, 25, 27, 36, 49, 64, ..]). Написать программу для
-- вычисления n-го члена этой последовательности.
-- копипаст (см. Хуснутдинов)
-- и то не рабочий. компильте перед тем как отправить, а то уж совсем как то нагло
-- получается, тк мне приходится доделывать за вами копипасты чтобы они заработали 
-- и их можно было прогнать.
sequence2_3_f :: Integer -> Integer
sequence2_3_f n = (merge3 (powers 2) (powers 3) facts) !! fromIntegral(n-1)

numbersFrom :: Integer -> [Integer]
numbersFrom n = n : (numbersFrom (n + 1))

powers :: Integer -> [Integer]
powers a = map (^a) (numbersFrom 1)

fac :: Integer -> Integer
fac 0 = 1
fac n = n * (fac (n - 1))

facts :: [Integer]
facts = map fac (numbersFrom 1)

merge2 :: Ord a => [a] -> [a] -> [a]
merge2 (x1:ls1) (x2:ls2) | x1 == x2  = x1 : (merge2 ls1 ls2)
    | x1 < x2   = x1 : (merge2 ls1 (x2:ls2))
    | otherwise = x2 : (merge2 (x1:ls1) ls2)

merge3 :: Ord a => [a] -> [a] -> [a] -> [a]
merge3 ls1 ls2 ls3 = merge2 ls1 (merge2 ls2 ls3)

-- |Написать функцию symmetric для проверки симметричности бинарного дерева.
-- Бинарное дерево является симметричным, если, проведя вертикальную линию
-- через корневой узел, правое поддерево будет являться зеркальным отражением
-- левого поддерева. Сравнивается лишь симметрия структуры дерева. Значение в
-- узле сравнивать не надо.
-- копипаст (см. Гатиатуллин)
data Tree a = Empty | Node (Tree a) a (Tree a)

symmetric ::  Tree a -> Bool
symmetric Empty = True
symmetric (Node leftTree _ rightTree) = compareTrees leftTree rightTree

compareTrees :: Tree a -> Tree a -> Bool
compareTrees Empty Empty = True
compareTrees Empty (Node _ _ _) = False
compareTrees (Node _ _ _) Empty  = False
compareTrees (Node aLeft _ aRight) (Node bLeft _ bRight) = compareTrees aLeft bLeft && compareTrees aRight bRight

-- |Написать функцию listDigits, которая для заданного дерева из строк выдает
-- список всех строк, содержащих хотя бы одну цифру.

listDigits :: Tree String -> [String]
listDigits Empty = []
listDigits (Node leftTree str rightTree) = listDigits(leftTree) ++ listDigits(rightTree) ++
    if containsDigits(str) then [str] else []

containsDigits :: String -> Bool
containsDigits str = any isDigit str

-- |Функция isHeap проверяет является ли дерево пирамидой, то есть значение
-- в каждом из его узлов меньше значений, хранящихся в поддеревьях у этого узла.
data MultiTree a = Branch a [MultiTree a] deriving (Show)

isHeap :: Ord a => MultiTree a -> Bool
isHeap (Branch _ []) = True
isHeap (Branch x xs) = all (\(Branch y _) -> x < y) xs && all isHeap xs