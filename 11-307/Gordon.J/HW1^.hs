module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , expon
       , hw1_2a
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
--hw1_1 a b = error "hw1_1 is not implemented"
hw1_1 a b =a+b
-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
expon :: Integer ->Integer
expon n = n^n
hw1_2a :: Integer -> Double
hw1_2a 1 = 1
hw1_2a n =1/fromIntegral(expon(n))
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 1 = 1
hw1_2 n = hw1_2a(n)+hw1_2(n-1)--hw1_2 n = error "hw1_2 not implemented yet"
-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
--fact2 n = error "fact2 not implemented yet"
-- зациклится при отрицательных (-0,5)
fact2 0 = 1
fact2 1 = 1
fact2 n = n*fact2(n-2)
-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isPrime :: Integer -> Bool
--isPrime p = error "isPrime is not implemented yet"
-- 1 тоже стоило включить в условие (-0,5)
isPrime p | p<=0      = False
          | otherwise = prime' 2 p      -- начинаем проверку с числа 2
prime' d p | d * d > p       = True   -- делители не найдены, число простое
           | p `mod` d == 0  = False  -- найден делитель, число не простое
           | otherwise       = prime' (d+1) p
sumPrime2 t n | isPrime n = t+n
              | otherwise = t
-- |Найти сумму всех простых чисел в диапазоне [a;b]
-- из предыдущего вытекает ошибка при подсчете от 1 (-0,5)
primeSum :: Integer -> Integer -> Integer
--primeSum a b = error "primeSum is not implemented yet"
primeSum a b | b<a = error "error"
             | b==a = sumPrime2 0 b
             | b>a =sumPrime2 0 b + primeSum a (b-1)
