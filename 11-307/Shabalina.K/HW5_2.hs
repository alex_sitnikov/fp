-- копипаст Гатитатуллина и то не полный. ни чего не работает. 
-- надо было скомпилить хотя бы прежде чем отправлять
module HW5_2
   ( bottom_up
   , internalPathLen
   , сonnectedСomponent
   , maxPath
   , bipartiteGraph
   ) where

import Data.List
import Data.Maybe

-- |Написать функцию bottom_up, которая построит список узлов дерева от листьев к корню.
data Tree a = Node a [Tree a] deriving (Show)

bottom_up  :: Tree a -> [a]
bottom_up node = reverse $ flatmap (\x -> x) (collectNodes node)

-- |Написать функцию internalPathLen, которая подсчитает сумму всех путей дерева от корня к листьям.
internalPathLen :: Tree a -> Int
internalPathLen (Node val []) = 1
internalPathLen (Node val xs) = sum $ map internalPathLen xs


-- |Функция paths должна вернуть список путей между 2мя точками в графе.
-- Каждый путь представляется собой список с вершинами от точки А до B.
-- Граф задается как список ребер [(a,a)]. Например, [(1,2),(2,3),(1,3),(3,4),(4,2),(5,6)]
--               А   B     граф
paths :: Eq a => a -> a -> [(a,a)] -> [[a]]
paths a b g = map (\path -> a : path) (pathsInternal a b g [a])
  where
    pathsInternal a b g visited
        | a == b    = [[]]
        | otherwise = [c:path | c <- neighbors a g, c `notElem` visited, path <- pathsInternal c b g (c:visited)]

