-- копипаст (см. Гатиатуллин)
module HW5_1
   ( rotateRight
   , sortList
 --  , multiPrimeFactor
--   , sphenicList
--   , calc
   ) where

import Data.List
import Data.Maybe


-- |Функция rotateRight принимает список произвольных данных и положительное число n
-- после чего производит циклическое вращение направо n раз. [a, b, c, d, e] -> [e, a, b, c, d]
--                      n
rotateRight :: [a] -> Int -> [a]
rotateRight xs 0     = xs
rotateRight [] _     = []
rotateRight xs n = rotateRight (last xs : init xs) (n - 1)


-- |Функция sortList принимает сортирует список по длине его подсписков.
sortList :: [[a]] -> [[a]]
sortList []   = []
sortList (l:ls) =
    let smallerThanHead = sortList [a | a <- ls, length a < length l]
        greaterThanHead = sortList [a | a <- ls, length a >= length l]
    in smallerThanHead ++ [l] ++ greaterThanHead










