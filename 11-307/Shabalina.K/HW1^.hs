--Последовательность Фибоначчи для n-ого члена
fib :: Integer -> Integer-- входной тип функции и выходной тип функции
fib n
   | n>2      = fib (n-1) + fib (n-2)--Фибоначи : каждый элемент есть сумма двух его предыдущих элементов
   | n<=0     = error "not defined" -- мы определяем n-ое число последовательности, и в данном контекте отсчет идет от 1ой позиции
   | otherwise= 1 --для 1 и 2 элемента базовый случай - 1, от которого и будет отталкиваться рекурсия
  
-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n | (n==2)&& (n `mod`2 == 0)     =2
        | (n==1) && (n `mod` 2 /= 0)   =1
		| n==0                         = error "число должно быть больше нуля"
		| n>0                          = n*(fact2 (n-2))
		
-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b

-- Использовать fromIntegral для перевода из Integer в Double
-- Задание заключалось в нахождении суммы ряда(fromIntegral лишь подсказка). На лекции я это говорил. (-0,5)
hw1_2 :: Integer -> Double
hw1_2 n = fromIntegral n 

--Вычислить , простое заданное число или нет
-- 1 не простое (-0,5)
prime                :: Integer -> Bool 
prime_two            :: Integer -> Integer -> Bool 
prime p       | p <= 0          =  error "Недопустимое отрицательное число"
              | otherwise       =  prime_two 2 p 
prime_two d p | d * d > p       =  True 
              | p `mod` d == 0  =  False
              | otherwise       =  prime_two (d+1) p 


-- |Найти сумму всех простых чисел в диапазоне [a;b]
-- сумма в интевале от 1 не правильна (-0,5)
summa  :: Integer -> Integer -> Integer
summa a b | (a == b && prime a== True) = a
          | (prime b == True)          = b + summa a (b-1)
          |  a==b                      = 0
          | (prime b == False)         = summa a (b-1)
	     
		                         
 --Вычислить сумму N членов ряда
multiplication :: Integer -> Integer
multiplication k = k * k
 
sigma :: Integer -> Double
calculation :: Integer -> Integer -> Double

sigma n | n==0      = error "not defined"
        | n>0       = calculation n n
 
calculation n 0 = 1
calculation n p = 1 / fromIntegral (multiplication p) + calculation n (p - 1)
		