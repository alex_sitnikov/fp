module HW2
       ( list2_1
       , gcd_1
       , coPrime
       , gcfList
       , perfect
       ) where   
-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Написать программу для нахождения минимального из
-- чисел, являющихся максимальными в каждой из строк заданной
-- прямоугольной матрицы.
list2_1 :: [[Integer]] -> Integer
list2_1 n = minimum (map (maximum) n)
--list2_1 n = error "list2_1 not implemented yet"

-- |Вычислить наибольший общий делитель(НОД) 2х натуральных чисел. Использовать алгоритм Евклида.
gcd_1 :: Integral a => a -> a -> a
gcd_1 n m | (m > n) = gcd_1 m n
          | ((n `mod` m) == 0) = m
          | otherwise = gcd_1 m (n `mod` m)
--gcd a b = error "gcd not implemented yet"

-- |Проверить являются ли 2 натуральных числа взаимнопростыми.
-- 2 числа называются взаимнопростыми если их НОД равен 1
coPrime :: Integer -> Integer -> Bool
coPrime n m | ((gcd_1 m n) == 1) = True
            | otherwise = False
--coPrime a b = error "coPrime is not implemented yet"

-- |Напишите функцию нахождения самого маленького числа, которое делится
-- на все числа от одного до N.
divBy :: Integer -> Integer -> [Integer]
divBy a n = map (mod a) [1..n]
isZero :: [Integer] -> Bool
isZero [] = True
isZero (x:xs) = x == 0 && isZero xs 
gcfList_1 :: Integer -> Integer -> Integer
gcfList_1 a n | isZero (divBy a n) = a
              | otherwise = gcfList_1 (a + 1) n             
gcfList :: Integer -> Integer
gcfList n = gcfList_1 n n  
--gcfList (x:ls) = error "gcfList is not implemented yet"

-- |Написать программу для нахождения первых N совершенных чисел.
-- Совершенным числом называется натуральное число, равное
-- сумме всех своих делителей, включая единицу, но исключая само
-- это число. Так, например, число 28 – совершенное, поскольку
-- 28 = 1 + 2 + 4 + 7 + 14. 
perfect :: Integer -> [Integer]
perfect n = perfectList n 1 0
divider_1 :: Integer -> Integer -> [Integer]
divider_1 a i | i > a - 1 = []
              | mod a i == 0 = i : divider_1 a (i + 1) 
              | otherwise = divider_1 a (i + 1)
divider :: Integer -> [Integer]
divider a = divider_1 a 1
isPerfect :: Integer -> Bool
isPerfect a = sum (divider a) == a
perfectList :: Integer -> Integer -> Integer -> [Integer]
perfectList n i k | k == n = []
                  | (isPerfect i) = i : perfectList n (i + 1) (k + 1)
                  | otherwise = perfectList n (i + 1) k
-- perfect a = error "perfect is not implemented yet"
