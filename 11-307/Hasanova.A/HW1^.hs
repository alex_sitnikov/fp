module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b -- сумма a и b

-- |Вычислить сумму N членов ряда
expon :: Integer -> Integer 
expon k = k ^ k -- возведение k в степень k
element :: Integer -> Double 
element k = 1 / fromIntegral (expon (k)) -- значение члена ряда
hw1_2 :: Integer -> Double 
hw1_2 n | n == 1 = 1 | n > 1 = element (n) + hw1_2 (n - 1) -- сумма ряда

-- |Вычислить двойной факториал
--  при отрицательныз зациклится (-0,5)
fact2 :: Integer -> Integer 
fact2 n | n == 1 = 1 -- при n = 1
        | n == 2 = 2 -- при n = 2
        | mod n 2 == 0 = n * fact2 (n - 2) -- если n - четное
        | mod n 2 == 1 = n * fact2 (n - 2) -- если n - нечетное

-- |Проверить заданное число на простоту
-- 1 не протое, надо было добавить на него проверку (-0,5)
isPrime :: Integer -> Bool 
isPrime' :: Integer -> Integer -> Bool 
isPrime p | p <= 0 = error "not prime" -- ошибка, если отрицательное
          | otherwise = isPrime' 2 p 
isPrime' d p | d * d > p = True  -- должно быть больше 2
             | p `mod` d == 0 = False -- не должно делиться на 2 без остатка
             | otherwise = isPrime' (d + 1) p  -- но 2 - простое

-- |Найти сумму всех простых чисел в диапазоне [a;b]
-- при подсчете от 1 результат не верный (-0,5) 
addPrime :: Integer -> Integer -> Integer 
addPrime t a | isPrime a == True = t + a 
             | isPrime a == False = t -- прибавление простого числа к сумме 
primeSum :: Integer -> Integer -> Integer 
primeSum a b | b == a = addPrime 0 b 
             | b > a = addPrime 0 b + primeSum a (b - 1) -- сумма всех простых чисел в сегменте