-- нет, extra_HW_1 за extra_HW_2 не прокатит (0)
-- опечатка "HW5_1" тривиальная. из описания там все ясно было.
module HW5_1
   ( rotateRight
   , sortList
   , multiPrimeFactor
   , sphenicList
   , calc
   ) where
 
import Data.List

rotateRight :: [a] -> Int -> [a]
rotateRight (h:t) n|n==0 =(h:t)
	               |otherwise =rotateRight (t ++ [h]) (n-1)

getItemMinLenght :: [a]->[a]->[a]

getItemMinLenght a b=if (length a)<=(length b) then a else b
sortList :: Eq a=>[[a]] -> [[a]]
sortList (x:xs)|length (x:xs)==1 =[x]
          |otherwise = 
          	let min=foldr1 getItemMinLenght (x:xs);
          	    list = delete min (x:xs);
          	in (min:sortList list)

multiPrimeFactor'::Int->Int ->[Int]->Int-> [(Int,Int)]
multiPrimeFactor' nN n (divH:divT) times|(divH<=n && mod n divH==0) =multiPrimeFactor' nN (div n divH) (divH:divT) (times+1)
                                     |(divH<=n && mod n divH/=0 && times==0) =multiPrimeFactor' nN n divT 0
                                     |(divH<=n && mod n divH/=0 && times>0) =((divH,times):(multiPrimeFactor' nN n divT 0))
                                     |n==1 =[(divH,times)]
                                     |otherwise = error ((show n)++" "++(show divH)++" "++(show times))

multiPrimeFactor :: Int -> [(Int,Int)]
multiPrimeFactor n= multiPrimeFactor' n n allPrimeNumbers 0

isOnceDividers :: [(Int,Int)]->Bool
isOnceDividers ((num,count):tl) = if count==1 then isOnceDividers tl else False
isOnceDividers [] = True

isSfenic :: Int -> Bool
isSfenic n = 
	let mltPrimeFactor=multiPrimeFactor n
	in if (length mltPrimeFactor ==3 && isOnceDividers mltPrimeFactor) then True else False
--				А		В
sphenicList :: Int -> Int -> [Int]
sphenicList a b|a<b = if (isSfenic a) then a:(sphenicList (a+1) b) else (sphenicList (a+1) b)
               |a==b =if (isSfenic a) then [a] else []

getResultBySign :: Int->Int->String->Int
getResultBySign frst scnd sign|sign=="+" =frst+scnd
                              |sign=="-" =frst-scnd
                              |sign=="*" =frst*scnd
                              |sign=="/" =div frst scnd
                              |sign=="%" =mod frst scnd
                              |sign=="^" =frst^scnd
                              |otherwise =frst

isPrime num
  | num <= 1 = False
  | otherwise =
  let 
  divisors=[x | x<-[1..num],mod num x == 0]
  in divisors == [1,num]

allPrimeNumbers = [2] ++ filter isPrime [3,5..] 