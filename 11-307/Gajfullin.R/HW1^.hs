- копипаст с Лавровой, либо наоборот. на почте оригинал похоже затерся=\
module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where


hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b


hw1_2 :: Integer -> Double
hw1_2 n |n==1 = 1
   |n>1 = summand(n) + hw1_2(n-1)

summand::Integer -> Double 
summand k=1/fromIntegral(k^k)


fact2 :: Integer -> Integer
fact2 n |n==1 = 1 
         |n==2 = 2 
         |mod n 2==0 = n*fact2(n-2) 
         |mod n 2==1 = n*fact2(n-2) 


        
isPrime :: Integer -> Bool
isPrime p = isPrime1 p 2

isPrime1::Integer -> Integer -> Bool 
isPrime1 n i |n<2 = False 
             |i*i>n = True 
             |mod n i==0 = False
             |otherwise = isPrime1 n (i+1)  


primeSum::Integer -> Integer -> Integer
primeSum a b |a>b = primeSum b a
               |a==b = addIfPrime a
               |a<b = addIfPrime a + primeSum b (a+1)

addIfPrime::Integer -> Integer
addIfPrime a |isPrime a==True = a
             |isPrime a==False = 0

