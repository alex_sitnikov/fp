﻿-- копипаст Зезеговой (ошибки см. у автора)
-- 1) Сумма двух аргументов
hw1_1::Integer->Integer->Integer 
hw1_1 a b= a+b 

-- 2) Сумма N членов ряда
expon::Integer->Integer
expon k=k^k
itemNumber::Integer->Double 
-- Вычисление элемента ряда (1/к^к)
itemNumber k=1/fromIntegral(expon(k))
hw1_2::Integer->Double 
hw1_2 n|n==1 = 1
-- Вычисление суммы ряда
|n>1 =itemNumber(n)+hw1_2(n-1) 

-- 3) Двойной факториал
hw1_3::Integer->Integer
-- если 1
hw1_3 n|n==1 =1
-- если 2
|n==2 =2  
-- если четный элемент
|mod n 2==0 =n*hw1_3(n-2) 
-- если нечетный элемент
|mod n 2==1 =n*hw1_3(n-2)  

-- 4) Проверка заданного числа на простоту
divisOnK::Integer->Integer->Bool
divisOnK n k|mod n k==0 =True
-- Проверка делимости n на k
|mod n k>0 =False  
-- при i=2
prost::Integer->Integer->Bool 
prost n i|n<1 =False 
|n==1 =True 
|n==2 =True 
|i*i>n =True 
|divisOnK n i==False =prost n (i+1)
-- Проверка на простоту
|divisOnK n i==True =False  
hw1_4::Integer->Bool 
hw1_4 n|prost n 2==False =False
-- Начинаем с 2-х
|prost n 2==True =True  

-- 5) Сумма всех простых чисел в диапазоне [a;b]
sumProst::Integer->Integer->Integer 
sumProst s a|hw1_4 a==True =s+a
-- Прибавление только простого числа 
|hw1_4 a==False =s
hw1_5::Integer->Integer->Integer 
hw1_5 a b|b==a =sumProst 0 b
-- Сумма всех простых чисел в диапазоне[a,b]
|b>a =sumProst 0 b + hw1_5 a (b-1) 