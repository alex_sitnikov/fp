-- копипаст (см. Мурзина)
-- просрочено (/2)
module HW2
       ( list2_1
       , gcdd
       , coPrime
--     , gcfList
--     , perfect
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Написать программу для нахождения минимального из
-- чисел, являющихся максимальными в каждой из строк заданной
-- прямоугольной матрицы.
findMax :: Integral a => [a] -> a
findMax [a] = a
findMax (a:n) = max a (findMax n)

findMin :: Integral a => [[a]] -> a
findMin [a] = findMax a
findMin (a:n) = min (findMax a) (findMin n)

list2_1 :: Integral a => [[a]] -> a
list2_1 [] = error "matrix is empty"
list2_1 n = findMin n

-- |Вычислить наибольший общий делитель(НОД) 2х натуральных чисел. Использовать алгоритм Евклида.
gcdd :: Integral a => a -> a -> a
gcdd a b | a == b = a
		 | a > b = gcdd (a-b) b
		 | otherwise = gcdd a (b-a)
	 	
-- |Проверить являются ли 2 натуральных числа взаимнопростыми.
-- 2 числа называются взаимнопростыми если их НОД равен 1
coPrime :: Integral a => a -> a -> Bool
coPrime a b | gcdd a b == 1 = True
			| otherwise = False