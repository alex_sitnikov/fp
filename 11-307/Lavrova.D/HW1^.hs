module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты


-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b


-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double

summand::Integer -> Double 
summand k=1/fromIntegral(k^k)

hw1_2 :: Integer -> Double
hw1_2 n 
   |n==1 = 1
   |n>1 = summand(n) + hw1_2(n-1)


-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
-- при отрицательных зациклится (-0,5)
fact2 :: Integer -> Integer
fact2 n 
   |n==1 = 1 
   |n==2 = 2 
   |mod n 2==0 = n*fact2(n-2) 
   |mod n 2==1 = n*fact2(n-2) 


-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
-- первый студент из 11-307 кто учел 1.. годнота
isPrime :: Integer -> Bool
isPrime p = isPrime1 p 2

isPrime1::Integer -> Integer -> Bool 
isPrime1 n i 
   |n<2 = False 
   |i*i>n = True 
   |mod n i==0 = False
   |otherwise = isPrime1 n (i+1)  


-- |Найти сумму всех простых чисел в диапазоне [a;b]

primeSum::Integer -> Integer -> Integer
primeSum a b 
   |a>b = primeSum b a
   |a==b = addIfPrime a
   |a<b = addIfPrime a + primeSum b (a+1)

addIfPrime::Integer -> Integer
addIfPrime a 
   |isPrime a==True = a
   |isPrime a==False = 0


