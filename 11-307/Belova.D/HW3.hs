-- �������� (��. ��������)
module HW3
       ( root
       , sequence2_3_f
       , symmetric
       , listDigits
       , isHeap
       ) where
import Data.List
import Data.Char   
-- |������� root ������ ��������� ������������ �������� ����� ���������
-- tan x = 1 - x � ���������, �������� ������ (� ������������) ����������
-- �������
f :: Double -> Double
f x = tan x + x - 1

root :: Double -> Double
root eps = decision eps 0 10

decision :: Double->Double->Double->Double
decision eps a1 a2|f a1 * f a2 > 0 =error "no roots"
                  |((f a1) < eps)&&((f a1) > -1*eps) =a1
                  |((f a2) < eps)&&((f a2) > -1*eps) =a2
                  |(f ((a2-a1)/2+a1) < eps)&&(f ((a2-a1)/2+a1) > -1*eps) =(a2-a1)/2+a1
                  |f a1 * f((a2-a1)/2+a1)<0 =decision eps a1 ((a2-a1)/2+a1)
                  |f a2 * f((a2-a1)/2+a1)<0 =decision eps ((a2-a1)/2+a1) a2
-- |����������� ������������� ������������������ ����� ����� ��� ����������
-- ���������� �� ���� ���������, ����� � ����������� ����������� �����
-- ([1, 2, 4, 8, 9, 16, 24, 25, 27, 36, 49, 64, ..]). �������� ��������� ���
-- ���������� n-�� ����� ���� ������������������.
sequence2_3_f :: Int -> Int
sequence2_3_f n = ((nub . sort) (merged))!!n
        where merged = mergeThree (take n squares) (take n cubes) (take n factorials)

squares = [x * x | x <- [1..]]
cubes = [x * x * x | x <- [1..]]
factorials = [x ^ x | x <- [1..]]

merge :: [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys) = x : y : merge xs ys

mergeThree xs ys zs = merge xs (merge ys zs)

-- |�������� ������� symmetric ��� �������� �������������� ��������� ������.
-- �������� ������ �������� ������������, ����, ������� ������������ �����
-- ����� �������� ����, ������ ��������� ����� �������� ���������� ����������
-- ������ ���������. ������������ ���� ��������� ��������� ������. �������� �
-- ���� ���������� �� ����.
data Tree a = Empty | Node (Tree a) a (Tree a)

symmetric ::  Tree a -> Bool
symmetric Empty = True
symmetric (Node leftTree _ rightTree) = compareTrees leftTree rightTree

compareTrees :: Tree a -> Tree a -> Bool
compareTrees Empty Empty = True
compareTrees Empty (Node _ _ _) = False
compareTrees (Node _ _ _) Empty  = False
compareTrees (Node aLeft _ aRight) (Node bLeft _ bRight) = compareTrees aLeft bLeft && compareTrees aRight bRight

-- |�������� ������� listDigits, ������� ��� ��������� ������ �� ����� ������
-- ������ ���� �����, ���������� ���� �� ���� �����.

data MultiTree a = Branch a [MultiTree a]

listDigits :: Tree String -> [String]
listDigits Empty = []
listDigits (Node leftTree str rightTree) = listDigits(leftTree) ++ listDigits(rightTree) ++
    if containsDigits(str) then [str] else []

containsDigits :: String -> Bool
containsDigits str = any isDigit str

-- |������� isHeap ��������� �������� �� ������ ���������, �� ���� ��������
-- � ������ �� ��� ����� ������ ��������, ���������� � ����������� � ����� ����.

isHeap :: Ord a => MultiTree a -> Bool
isHeap (Branch _ []) = True
isHeap (Branch x xs) = all (\(Branch y _) -> x < y) xs && all isHeap xs