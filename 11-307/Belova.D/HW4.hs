-- копипаст Мурзин
module HW4
   ( insertValue
   , slice
   , collectNodes
   , isEulerPathExist
   , paths
   ) where
    
-- |Функция insertValue должна должна вставлять значение в список на указанную 
-- позицию. 
insertValue :: a -> [a] -> Int -> [a]
insertValue a b n=((take n b) ++ [a]) ++ (drop n b)

-- |Функция slice должна возвращать подсписок в пределах от A до B 
--		список	 А		B
slice :: [a] -> Int -> Int -> Maybe [a]
slice a aa bb= 
    if (length (take (bb-aa) (drop (aa-1) a)) ==0) then Nothing
        else Just (take (bb-aa) (drop (aa-1) a))


-- |Написать функцию collectNodes, которая для каждого уровня дерева составит список
-- значений узлов и выведет общий список всех уровней.
data Tree a = Node a [Tree a]
takeNode :: Tree a -> a
takeNode (Node a _ ) = a
takeBranches::Tree a->[Tree a]
takeBranches (Node _ a)=a 
collect :: [Tree a] -> [[a]]
collect a = 
    let nodes= (map (takeNode) a);
        branches= concat (map (takeBranches) a)
        in if length branches >0 then (nodes:collect branches)
        else [nodes]
collectNodes :: Tree a -> [[a]]
collectNodes (Node a b)=[a]:(collect b) 
-- |Функция isEulerPathExist должна проверить содержит ли граф Эйлеров путь 
type Graph = [(Int, [Int])]
getVerticesCount:: Graph -> Int
getVerticesCount ((x,t):s)= 
    if mod (length t) 2 >0 then 1 + (getVerticesCount s)
    else (getVerticesCount s)
isEulerPathExist :: Graph -> Bool
isEulerPathExist a = 
    if getVerticesCount a > 2 then False
        else True
-- |Функция paths должна вернуть список путей между 2мя точками в графе.
-- Каждый путь представляется собой список с вершинами от точки А до B.
-- Граф задается как список ребер [(a,a)]. Например, [(1,2),(2,3),(1,3),(3,4),(4,2),(5,6)]
--				 А	  B		граф		
paths :: Eq a => a -> a -> [(a,a)] -> [[a]]
paths = error "paths is not implemented"