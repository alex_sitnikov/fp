-- копипаст (см. Мурзин)

module HW5_1
   ( rotateRight
   , sortList
   , multiPrimeFactor
   , sphenicList
   , calc
   ) where
 
import Data.List
-- |Функция rotateRight принимает список произвольных данных и положительное число n
-- после чего производит циклическое вращение направо n раз. [a, b, c, d, e] -> [e, a, b, c, d] 
--						n
rotateRight :: [a] -> Int -> [a]
rotateRight (h:t) n|n==0 =(h:t)
	               |otherwise =rotateRight (t ++ [h]) (n-1)

getItemMinLenght :: [a]->[a]->[a]

getItemMinLenght a b=if (length a)<=(length b) then a else b
-- |Функция sortList принимает сортирует список по длине его подсписков.
sortList :: Eq a=>[[a]] -> [[a]]
sortList (x:xs)|length (x:xs)==1 =[x]
          |otherwise = 
          	let min=foldr1 getItemMinLenght (x:xs);
          	    list = delete min (x:xs);
          	in (min:sortList list)

multiPrimeFactor'::Int->Int ->[Int]->Int-> [(Int,Int)]
multiPrimeFactor' nN n (divH:divT) times|(divH<=n && mod n divH==0) =multiPrimeFactor' nN (div n divH) (divH:divT) (times+1)
                                     |(divH<=n && mod n divH/=0 && times==0) =multiPrimeFactor' nN n divT 0
                                     |(divH<=n && mod n divH/=0 && times>0) =((divH,times):(multiPrimeFactor' nN n divT 0))
                                     |n==1 =[(divH,times)]
                                     |otherwise =error ((show n)++" "++(show divH)++" "++(show times))
-- |Функция multiPrimeFactor по заданному числу стоит список из его простых делителей
-- и количества их вхождений. Список сортируется в порядке возрастания простых делителей.
--							prime,count
multiPrimeFactor :: Int -> [(Int,Int)]
multiPrimeFactor n= multiPrimeFactor' n n allPrimeNumbers 0

isOnceDividers :: [(Int,Int)]->Bool
isOnceDividers ((num,count):tl) = if count==1 then isOnceDividers tl else False
isOnceDividers [] = True
isSfenicNumber :: Int -> Bool
isSfenicNumber n = 
	let mltPrimeFactor=multiPrimeFactor n
	in if (length mltPrimeFactor ==3 && isOnceDividers mltPrimeFactor) then True else False
-- |Функция sphenicList возвращается список сфенических чисел в промежутке от А до В.
-- ru.wikipedia.org/wiki/Сфеническое_число
--				А		В
sphenicList :: Int -> Int -> [Int]
sphenicList a b|a<b = if (isSfenicNumber a) then a:(sphenicList (a+1) b) else (sphenicList (a+1) b)
               |a==b =if (isSfenicNumber a) then [a] else []

getResultBySign :: Int->Int->String->Int
getResultBySign frst scnd sign|sign=="+" =frst+scnd
                              |sign=="-" =frst-scnd
                              |sign=="*" =frst*scnd
                              |sign=="/" =div frst scnd
                              |sign=="%" =mod frst scnd
                              |sign=="^" =frst^scnd
                              |otherwise =frst

calc' :: [String]->Int->Int->String->Bool->Int
calc' exp frst scnd sign frstNmbr|(length exp)==0 =(getResultBySign frst scnd sign)
	|otherwise =case (head exp) of
			"one" -> if frstNmbr then calc' (tail exp) (frst*10+1) scnd sign True else calc' (tail exp) frst (scnd*10+1) sign False
			"two" -> if frstNmbr then calc' (tail exp) (frst*10+2) scnd sign True else calc' (tail exp) frst (scnd*10+2) sign False
			"three" -> if frstNmbr then calc' (tail exp) (frst*10+3) scnd sign True else calc' (tail exp) frst (scnd*10+3) sign False
			"four" -> if frstNmbr then calc' (tail exp) (frst*10+4) scnd sign True else calc' (tail exp) frst (scnd*10+4) sign False
			"five" -> if frstNmbr then calc' (tail exp) (frst*10+5) scnd sign True else calc' (tail exp) frst (scnd*10+5) sign False
			"six" -> if frstNmbr then calc' (tail exp) (frst*10+6) scnd sign True else calc' (tail exp) frst (scnd*10+6) sign False
			"seven" -> if frstNmbr then calc' (tail exp) (frst*10+7) scnd sign True else calc' (tail exp) frst (scnd*10+7) sign False
			"eight" -> if frstNmbr then calc' (tail exp) (frst*10+8) scnd sign True else calc' (tail exp) frst (scnd*10+8) sign False
			"nine" -> if frstNmbr then calc' (tail exp) (frst*10+9) scnd sign True else calc' (tail exp) frst (scnd*10+9) sign False
			"zero" -> if frstNmbr then calc' (tail exp) (frst*10) scnd sign True else calc' (tail exp) frst (scnd*10) sign False
			"+" -> calc' (tail exp) (getResultBySign frst scnd sign) 0 "+" False
			"-" -> calc' (tail exp) (getResultBySign frst scnd sign) 0 "-" False
			"*" -> calc' (tail exp) (getResultBySign frst scnd sign) 0 "*" False
			"/" -> calc' (tail exp) (getResultBySign frst scnd sign) 0 "/" False
			"%" -> calc' (tail exp) (getResultBySign frst scnd sign) 0 "%" False
			"^" -> calc' (tail exp) (getResultBySign frst scnd sign) 0 "^" False
			_ -> error (head exp)


-- |Функция  подсчитывает значение произвольного выражения записанного в символьной форме.
-- Пример, ["one","two","one","+","six","*","two","six"] -> (121 + 6) * 26 = 3302
-- Определенные операторы: +, -, *, /, % (деление по модулю) , ^ (степень числа)
-- Приоритета операций нет, потому все вычисления производятся слева направо последовательно.
calc :: [String] -> Int
calc exp = calc' exp 0 0 "" True

isPrime num
  | num <= 1 = False
  | otherwise =
  let 
  divisors=[x | x<-[1..num],mod num x == 0]
  in divisors == [1,num]

allPrimeNumbers = [2] ++ filter isPrime [3,5..] 