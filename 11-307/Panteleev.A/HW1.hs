-- Ужасный копипаст Зезеговой, лень было форматировать, потому взял с оригинала. (ошибки см. у автора)
-- задание 1
hw1_1::Integer->Integer->Integer 
hw1_1 a b= a+b  

-- задание 2
expon::Integer->Integer 
expon k=k^k
-- — нахождение к в степени к 

itemNumber::Integer->Double 
itemNumber k=1/fromIntegral(expon(k)) 
-- нахождение элемента ряда, 1/к^к 

hw1_2::Integer->Double 
hw1_2 n 
        | n==1 = 1 
        |n>1 =itemNumber(n)+hw1_2(n-1)
-- — нахождение суммы ряда 

-- задание 3, факториал 
-- виснет на неположительных числах
hw1_3::Integer->Integer 
hw1_3 n
	|n==1 =1
--	— если 1 
	|n==2 =2
--	— если 2 
	|mod n 2==0 =n*hw1_3(n-2)
-- — если четный элемнт 
	|mod n 2==1 =n*hw1_3(n-2)
-- — если нечетный элемент 

-- задание 4
divisOnK::Integer->Integer->Bool 
divisOnK n k
	|mod n k==0 =True 
	|mod n k>0 =False
-- — проверка на делимость n на k 

--i=2 
prost::Integer->Integer->Bool 
prost n i
	|n<1 =False 
	|n==1 =True 
	|n==2 =True 
	|i*i>n =True 
	|divisOnK n i==False =prost n (i+1) 
	|divisOnK n i==True =False
-- — простое ли число

-- 1 не относится к простым числам
hw1_4::Integer->Bool 
hw1_4 n
	|prost n 2==False =False 
	|prost n 2==True =True
-- — простое ли число (i=2)

-- задание 5
sumProst::Integer->Integer->Integer 
sumProst s a
	|hw1_4 a==True =s+a 
	|hw1_4 a==False =s
-- — прибавление только простого числа 

hw1_5::Integer->Integer->Integer 
hw1_5 a b
	|b==a =sumProst 0 b 
	|b>a =sumProst 0 b + hw1_5 a (b-1)
-- — сумма всех простых чисел из диапазона[a,b]