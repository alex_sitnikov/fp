module HW2
       ( list2_1
       , gcdMy
       , coPrime
       , gcfList
       , perfect
       ) where
-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Написать программу для нахождения минимального из
-- чисел, являющихся максимальными в каждой из строк заданной
-- прямоугольной матрицы.
list2_1 :: [[Integer]] -> Integer
list2_1 n = minimum (map (maximum) n)
--list2_1 n = error "list2_1 not implemented yet"

-- |Вычислить наибольший общий делитель(НОД) 2х натуральных чисел. Использовать алгоритм Евклида.
gcdMy :: Integral a => a -> a -> a
gcdMy n m | (m > n) = gcdMy m n
        | ((n `mod` m) == 0) = m
        | otherwise = gcdMy m (n `mod` m)
--gcd a b = error "gcd not implemented yet"

-- |Проверить являются ли 2 натуральных числа взаимнопростыми.
-- 2 числа называются взаимнопростыми если их НОД равен 1
coPrime :: Integer -> Integer -> Bool
coPrime n m | ((gcdMy m n)==1)=True
            | otherwise = False
--coPrime a b = error "coPrime is not implemented yet"

-- |Напишите функцию нахождения самого маленького числа, которое делится
-- на все числа от одного до N.

delit ::Integer->Integer->Integer
delit a b =a `div` (gcdMy a b)

nok :: Integer->Integer->Integer
nok a b=delit a b * gcdMy a b *delit b a
 
hw_4 ::Integer->Integer->Integer
hw_4 i n |i<n-1 =nok i (hw_4 (i+1) n)
		 |i==n-1 =nok (n-1) n

gcfList ::[Integer]->Integer
gcfList n = hw_4 1 (last n)

--gcfList (x:ls) = error "gcfList is not implemented yet"

-- -- |Написать программу для нахождения первых N совершенных чисел.
-- -- Совершенным числом называется натуральное число, равное
-- -- сумме всех своих делителей, включая единицу, но исключая само
-- -- это число. Так, например, число 28 – совершенное, поскольку
-- -- 28 = 1 + 2 + 4 + 7 + 14. 
-- list = []
-- Plist =[]
perfect :: Integer -> [Integer]
perfect n = perfectList n 1

isPerfect :: Integer->Bool
isPerfect a =del 1 a == a

del::Integer->Integer->Integer
del i a |i<a =massive i a + del (i+1) a
		|i==a =0
		
massive::Integer->Integer->Integer
massive i a |divisOnK a i==True =i
			|divisOnK a i==False =0
			
divisOnK::Integer->Integer->Bool
divisOnK n k|mod n k==0 =True
			|mod n k>0 =False

perfectList :: Integer -> Integer->[Integer]
perfectList n i| i>n =[]
               | (isPerfect i) = i : perfectList n (i+1)
               | otherwise = perfectList (n+1) (i+1)

-- perfect a = error "perfect is not implemented yet"
