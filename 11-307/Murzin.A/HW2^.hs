module HW2
       ( list2_1
       , gcdd
       , coPrime
       , gcfList
 --      , perfect
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Написать программу для нахождения минимального из
-- чисел, являющихся максимальными в каждой из строк заданной
-- прямоугольной матрицы.
findMax :: Integral a=>[a]->a
findMax [n] = n
findMax (a:n) = max a (findMax n) 

maxMin :: Integral a=> [[a]]->a
maxMin [n] = findMax n
maxMin (a:n) = min (findMax a) (maxMin n)

list2_1 :: Integral a => [[a]] -> a
list2_1 [] = error "matrix is empty"
list2_1 n = maxMin n

-- |Вычислить наибольший общий делитель(НОД) 2х натуральных чисел. Использовать алгоритм Евклида.
gcdd :: Integral a => a -> a -> a
gcdd a b = 
	if a==b 
		then a
		else 
			if a > b then (gcdd (a-b) b) else (gcdd a (b-a))
	 	

-- |Проверить являются ли 2 натуральных числа взаимнопростыми.
-- 2 числа называются взаимнопростыми если их НОД равен 1
coPrime :: Integral a => a -> a -> a
coPrime a b = gcdd a b

-- |Напишите функцию нахождения самого маленького числа, которое делится
-- на все числа от одного до N.
-- задача решена не верно. вы просто проверяете являются ли все чиста из списка делителями N
-- по исходному условию исходное N вам не ивестно и его надо найти, те НОК списка чисел.
-- на паре я это говорил (-1)
isDelitsyaNaN :: Integral a=> a -> a ->Bool
isDelitsyaNaN a n = 
	if (n==0) 
		then False
		else
			if (n==1) 
				then True 
				else
					if ((mod a n) == 0)
				    	then (isDelitsyaNaN a (n-1)) 
						else False

gcfList :: Integral a => [a] -> a -> a
gcfList (x:ls) n = if (isDelitsyaNaN x n) then (min x (gcfList ls n)) else (gcfList ls n)
	

-- |Написать программу для нахождения первых N совершенных чисел.
-- Совершенным числом называется натуральное число, равное
-- сумме всех своих делителей, включая единицу, но исключая само
-- это число. Так, например, число 28 – совершенное, поскольку
-- 28 = 1 + 2 + 4 + 7 + 14. 
--perfect :: Integer a => a -> [a]
--perfect a = error "perfect is not implemented yet"