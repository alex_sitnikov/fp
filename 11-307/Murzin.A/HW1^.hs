module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1::Integer->Integer->Integer 
hw1_1 a b= a+b 

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 n|n==1 =1
       |n>1 =1/fromIntegral(n^n)+hw1_2(n-1)

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетноеx
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n|n==1 =1
       |n==2 =2
       |n>0 =n*fact2(n-2)
-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
prost::Integer -> Integer -> Bool
prost n i|i*i>n =True
         |mod n i==0 =False
         |mod n i>0 =prost n (i+1)

-- 1 - не простое (-0,5)
isPrime :: Integer -> Bool
isPrime p|p==1 =True
         |p==2 =True
         |p>2 =prost p 2

-- |Найти сумму всех простых чисел в диапазоне [a;b]
-- ошибка при подсчете от 1 (-0,5)
getProst ::Integer -> Integer
getProst n|isPrime n==True =n
          |otherwise =0
primeSum :: Integer -> Integer -> Integer
primeSum a b|a==b =getProst a
            |a<b =getProst a + primeSum (a+1) b

