-- �������� (��. �����������)
module HW4
   ( insertValue
   , slice
   , collectNodes
   , isEulerPathExist
   , paths
   ) where
    
-- |������� slice ������ ���������� ��������� � �������� �� A �� B 
--		������	 �		B
-- ��� ������: �� ���� �������� ������ xs � �������� p. ������������ ���������� ���������, �������� �������� �������� ������������� p.
slice :: [a] -> Int -> Int -> Maybe [a]
slice p []  = []
slice p [a] = [a]
slice p (a:xs) = a : slice' a xs
  where
    slice' s ys = case dropWhile (p s) ys of
      [] -> []
      (y:ys') -> y : slice' y ys'

	  
	  
	  
-- |������� paths ������ ������� ������ ����� ����� 2�� ������� � �����.
-- ������ ���� �������������� ����� ������ � ��������� �� ����� � �� B.
-- ���� �������� ��� ������ ����� [(a,a)]. ��������, [(1,2),(2,3),(1,3),(3,4),(4,2),(5,6)]
--				 �	  B		����		
-- start - ��������� �����, goal - ��������
-- ���� start == goal, �� ���������� [[start]]
-- ���� start ����������� � ������� ���������� ����� (=visited), �� ��� ������� �������� ������� (=next) ���� ���� �� ����� ������ � goal.
-- ����� ���������� [] (�.�. ���� �� ��� �������� start)
-- ���������� ������� ���������� ����� �� ������ ���� ����������� � ������� paths'
paths :: Eq a => a -> a -> [(a,a)] -> [a]
paths cm start goal = paths' [] cm start goal
	where paths' visited cm start goal
		| start == goal 	  = [[start]]
		| start `notElem` visited = [start:rest | next <- neighbours cm start,
												  rest <- paths' (start:visited) cm next goal]
		| otherwise 		  = []
		
		
		
		
-- |������� insertValue ������ ������ ��������� �������� � ������ �� ��������� 
-- �������.
insertValue :: a -> [a] -> Int -> [a]
insertValue = error "insertValue is not implemented"
		
-- |�������� ������� collectNodes, ������� ��� ������� ������ ������ �������� ������
-- �������� ����� � ������� ����� ������ ���� �������.
data Tree a = Node a [Tree a]

collectNodes :: Tree a -> [[a]]
collectNodes = error "collectNodes is not implemented"

-- |������� isEulerPathExist ������ ��������� �������� �� ���� ������� ���� 
type Graph = [(Int, [Int])]

isEulerPathExist :: Graph -> Bool
isEulerPathExist = error "isEulerPathExist is not implemented"
