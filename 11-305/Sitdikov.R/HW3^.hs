-- ������ ���� - ������, �� ��������� - ����� (/2)
module HW3
       ( root
       , sequence2_3_f
       , symmetric
       , listDigits
       , isHeap
       ) where

import Data.List
import Data.Char

-- |������� root ������ ��������� ������������ �������� ����� ���������
-- tan x = 1 - x � ���������, �������� ������ (� ������������) ����������
-- �������
f :: Double -> Double
f x = tan x + x - 1

root :: Double -> Double
-- � ����� ����������� ������� ���������, � �� ��� �������� �����������
-- ���� � ������ ������ �� ��������� � �������� ������� �� ������, ��
-- ��������� �� ����� (-0,5)
root eps = head [i | i <- [0, eps..], abs (f i) < eps]

-- |����������� ������������� ������������������ ����� ����� ��� ����������
-- ���������� �� ���� ���������, ����� � ����������� ����������� �����
-- ([1, 2, 4, 6, 8, 9, 16, 24, 25, 27, 36, 49, 64, ..]). �������� ��������� ���
-- ���������� n-�� ����� ���� ������������������.
sequence2_3_f :: Int -> Integer
sequence2_3_f n = sort (take (n^3) (1 : (foldr (++) [] [[x*x, x*x*x, product [1..x]] | x <- [2..]]))) !! (n - 1)

-- |�������� ������� symmetric ��� �������� �������������� ��������� ������.
-- �������� ������ �������� ������������, ����, ������� ������������ �����
-- ����� �������� ����, ������ ��������� ����� �������� ���������� ����������
-- ������ ���������. ������������ ���� ��������� ��������� ������. �������� �
-- ���� ���������� �� ����.
data Tree a = Empty | Node (Tree a) a (Tree a)
symmetric ::  Tree a -> Bool
symmetric' :: Tree a -> Tree b -> Bool

symmetric (Node a _ b) = symmetric' a b
symmetric' Empty Empty = True
symmetric' (Node l _ r) (Node a _ b) = symmetric' l b && symmetric' r a
symmetric' _ _ = False

-- |�������� ������� listDigits, ������� ��� ��������� ������ �� ����� ������
-- ������ ���� �����, ���������� ���� �� ���� �����.
listDigits :: Tree String -> [String]
listDigits tree = [string | string <- flatten tree, check string]

flatten :: Tree a -> [a]
flatten Empty = []
flatten (Node t1 n t2) = (flatten t1) ++ (n : (flatten t2))

check :: String -> Bool
check string = foldr (||) False (map (\x -> isDigit x) string)

-- |������� isHeap ��������� �������� �� ������ ���������, �� ���� ��������
-- � ������ �� ��� ����� ������ ��������, ���������� � ����������� � ����� ����.
data MultiTree a = Branch a [MultiTree a]

isHeap :: Ord a => MultiTree a -> Bool
isHeap (Branch x t) = all f t 
	where f n@(Branch y _) = x<y && isHeap n
