import Data.List
import System.IO


list2_1 :: Integral a => [[a]] -> a
list2_1 arr = minimum [maximum x| x <- arr]

gcd' :: Integral a => a -> a -> a
gcd' a b |a <= 0 || b < 0 = error "wrong arguments format"
 	 |b == 0 = a
  	 |otherwise = gcd' b (mod a b)

coPrime :: Integral a => a -> a -> Bool
coPrime a b |gcd' a b == 1 = True
	    |otherwise = False

gcfList :: Integral a => a -> a
gcfList 1 = 1
gcfList n = div (gcfList(n-1) * n) (gcd' (gcfList (n-1)) n)


perfect :: Integral a => a -> [a]
perfect a = take (fromIntegral a) [b | b <- [1..], b == (sum [i | i <- [1..div b 2], mod b i == 0])]
