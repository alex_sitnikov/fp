-- частичный копипаст (см. Филиппов)
insertValue :: a -> [a] -> Int -> [a]
insertValue a b c = (take c b) ++ [a] ++ reverse (take (length b - c) (reverse b))

slice :: [a] -> Int -> Int -> Maybe [a]
slice a b c | (c >= b) = Just (take (c - b + 1) (drop b a))
      | otherwise = Nothing     


data Tree a = Node a [Tree a]

getNode :: Tree a -> a
getNode (Node a tr) = a

getTrees :: Tree a -> [Tree a]
getTrees (Node a tr) = tr


collectNodes :: Tree a -> [[a]]
collectNodes (Node a tr) = ([a] :) $ collectNodes' ((Node a tr):[])

collectNodes' :: [Tree a] -> [[a]]
collectNodes' tr = let childTrees = concat (map getTrees tr)
                   in if null childTrees then [] else (map getNode childTrees) : (collectNodes' childTrees)



type Graph = [(Int, [Int])]

isEulerPathExist :: Graph -> Bool
isEulerPathExist cs = w == 0 || w == 2 
                        where w = length (filter ((/= 0) . (mod 2) . length . snd) cs)

	
paths :: Eq a => a -> a -> [(a,a)] -> [[a]]
paths = buildPaths []
    
buildPaths :: Eq a => [a] -> a -> a -> [(a, a)] -> [[a]]
buildPaths builded st fin _ | (st == fin) = (fin:builded) : []
buildPaths builded st fin g = let stack = map (\s -> fst s) $ filter (\x -> (snd x) == fin && elem (fst x) builded == False) g
                              in if   (null stack) 
								 then []
								 else (concatMap (\s -> buildPaths (fin:builded) st s g) stack)