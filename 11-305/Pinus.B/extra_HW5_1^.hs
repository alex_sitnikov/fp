import Data.List

rotateRight :: [a] -> Int -> [a]
rotateRight st x = take (length st) $ drop (negate x `mod` length st) $ cycle st

primes :: [Int]
primes = sieve [2..]
	where
		sieve (p:xs) = p : sieve [x|x <- xs, x `mod` p > 0]

divisors::Int -> [Int]
divisors n = [x | x <- [2..(n-1)], n `rem` x == 0]

primeDivisors::Int ->Int -> [Int]
primeDivisors x b = intersect (divisors x) (take (head [ i | i <- [0..], primes!!i > b]) primes)

sphenicList :: Int -> Int -> [Int]
sphenicList  a b =  [x | x <- [a..b],  length (primeDivisors x b) == 3 && (foldr (\x i -> x*i) 1 (primeDivisors x b)) == x ]



sortList :: [[a]] -> [[a]]
sortList [] = []
sortList (x:xs) = sortList small ++ (x : sortList large)
	where
	 	small = [y | y <- xs, length y <= length x]
		large = [y | y <- xs, length y > length x]