import Data.List
import System.IO


hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b

-- сумма ряда 1/n. ряд расходится (-0,5) 
hw1_2 :: Integer -> Double
hw1_2 n = sum (map (1/) [1.. fromIntegral n])

-- проверки на отрицательные нет. зацикливается. (-0,5)
fact2 :: Integer -> Integer
fact2 1 = 1
fact2 2 = 2
fact2 n  = n * fact2(n-2)

-- 1 не простое (-0,5)
isPrime :: Integer -> Bool
helper :: Integer-> Integer -> Bool
isPrime 2 = True
isPrime n = helper n 2
helper n a | mod n a == 0 = False
		   | a * a > n = True
           |  mod n a /= 0 = helper n (a + 1)

-- сумма от 1 считается не верно (-0,5)
primeSum :: Integer -> Integer -> Integer
primeSum a b = sum (filter (isPrime) [a..b])