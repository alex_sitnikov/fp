-- �������� (��. ��������)
-- ���������� ������ (/4)
module HW3
       ( root
       , sequence2_3_f
       , symmetric
       , listDigits
       --, isHeap
       ) where
import Prelude
import Control.Applicative
import Data.Set (fromList, elemAt, deleteAt, Set)
import Data.List
import Data.Char
       
-- |������� root ������ ��������� ������������ �������� ����� ���������
-- tan x = 1 - x � ���������, �������� ������ (� ������������) ����������
-- �������
f :: Double -> Double
f x = 1 - tan x

root :: Double -> Double
root eps = iterCalc eps 1e0 f

--Function that represents single iteration of contracting mapping
iterCalc :: Double -> Double -> (Double -> Double) -> Double
iterCalc eps x fun = let y = fun x
                     in if abs(y - x) < eps then y
                                            else iterCalc eps y fun


-- |����������� ������������� ������������������ ����� ����� ��� ����������
-- ���������� �� ���� ���������, ����� � ����������� ����������� �����
-- ([1, 2, 4, 8, 9, 16, 24, 25, 27, 36, 49, 64, ..]). �������� ��������� ���
-- ���������� n-�� ����� ���� ������������������.
sequence2_3_f :: Integer -> Integer
sequence2_3_f n = (elemAt 0) $ deletes (n-1) (makeSequences n)

--finds the n'th element of sequence
deletes :: Integer -> Set a -> Set a
deletes n set | (n == 0) = set
              | True     = deletes (n-1) (deleteAt 0 set)

--This function produces produce the above sequence by making set from concatenated 
--lists of (^2), (^3) and x! members               
makeSequences :: Integer -> Set Integer
makeSequences n = fromList ([(^2), (^3), (\x -> product [1..x])] <*> [1..n])

-- |�������� ������� symmetric ��� �������� �������������� ��������� ������.
-- �������� ������ �������� ������������, ����, ������� ������������ �����
-- ����� �������� ����, ������ ��������� ����� �������� ���������� ����������
-- ������ ���������. ������������ ���� ��������� ��������� ������. �������� �
-- ���� ���������� �� ����.
data Tree a = Empty | Node (Tree a) a (Tree a)

symmetric :: Tree a -> Bool
symmetric Empty          = True
symmetric (Node a1 a a2) = eqRoots a1 a2 

--Function of folding the Tree on given function by left-side order
foldTree  ::  (a -> b -> b) -> b -> Tree a -> b
foldTree _ seed Empty           =  seed
foldTree f seed (Node t1 n t2)  =  foldTree f (f n (foldTree f seed t2)) t1

--Make a list from given Tree
flatten ::  Tree a -> [a]
flatten t  =  foldTree (:) [] t

--Fuction of eqality of two Nodes. 
--Returns true if both of surveied nodes in ledt side and right side 
--are Empty or real Nodes in each level of Tree. 
--Returns false otherwise.
eqRoots :: Tree a -> Tree b -> Bool
eqRoots a b = case (a, b) of
                  (Empty, Empty)                     -> True
                  (Node t11 n1 t12, Node t21 n2 t22) -> (eqRoots t11 t21) && (eqRoots t12 t22)
                  (_, _)                             -> False
                
-- |�������� ������� listDigits, ������� ��� ��������� ������ �� ����� ������
-- ������ ���� �����, ���������� ���� �� ���� �����.
listDigits :: Tree String -> [String]
listDigits tree = filter containsDigit (flatten tree)

--Returns true if at least one of 0..9 digit character will be found in given String
containsDigit :: String -> Bool
containsDigit s = let ds = map intToDigit [0..9]
                  in or $ (map (\c -> containsChar c s)) ds
                  
containsChar :: Char -> String -> Bool
containsChar = any . (==)

data MultiTree a = Branch a [MultiTree a]

-- |������� isHeap ��������� �������� �� ������ ���������, �� ���� ��������
-- � ������ �� ��� ����� ������ ��������, ���������� � ����������� � ����� ����.

isHeap :: Ord a => MultiTree a -> Bool
isHeap (Branch b [])  = True
isHeap (Branch b sub) = if y then and (map isHeap sub) else False
                            where y = filterMTree (b<) True sub

--Analogue of filtering by predicte the list of Multitrees
filterMTree :: (a -> Bool) -> Bool -> [MultiTree a] -> Bool
filterMTree p bol [] = bol
filterMTree p bol ((Branch b sub):bs) = filterMTree p ((&&) bol $ p b) bs

