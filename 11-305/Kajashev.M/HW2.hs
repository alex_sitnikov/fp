-- Каяшев после покипаста хотя бы компиль для приличия
module HW2
       ( list2_1
       , gcd
       , coPrime
       , gcfList
       , perfect
       ) where
import Prelude

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Написать программу для нахождения минимального из
-- чисел, являющихся максимальными в каждой из строк заданной
-- прямоугольной матрицы.
list2_1 :: Integral a => [[a]] -> a
list2_1 n = minimum (maxInLine n)

maxInLine :: Integral a => [[a]] -> [a]
maxInLine n = map (maximum) n

-- |Вычислить наибольший общий делитель(НОД) 2х натуральных чисел. Использовать алгоритм Евклида.
-- собрал бы и заметил, что пытаешься переопределить НОД из Prelude (-0,5)
gcd1 :: Integral a => a -> a -> a
gcd1 a b | c == 0     = b
        | otherwise  = gcd1 b c
    where c = a `mod` b

-- |Проверить являются ли 2 натуральных числа взаимнопростыми.
-- 2 числа называются взаимнопростыми если их НОД равен 1
coPrime :: Integral a => a -> a -> Bool
coPrime a b | gcd a b == 1     = True
            | otherwise        = False

-- |Напишите функцию нахождения самого маленького числа, которое делится
-- на все числа от одного до N.
gcfList :: (Num a, Integral a) => [a] -> a
gcfList (x:ls) = gcfListE x ls

gcfListE :: (Num a, Integral a) => a -> [a] -> a
gcfListE e [] = e
gcfListE e (x:ls) = gcfListE (e * x `div` (gcd e x)) ls

-- |Написать программу для нахождения первых N совершенных чисел.
-- Совершенным числом называется натуральное число, равное
-- сумме всех своих делителей, включая единицу, но исключая само
-- это число. Так, например, число 28 – совершенное, поскольку
-- 28 = 1 + 2 + 4 + 7 + 14. 
perfect :: Integer -> [Integer]
perfect a = perfectList 1 a [] 1

perfectList :: Integer -> Integer -> [Integer] -> Integer -> [Integer]
perfectList c n ls counter | counter > n = ls
                        | prime c2    = c2 * ((c2 + 1) `div` 2) : (next (counter+1)) 
                        | otherwise   = next counter
    where c2 = 2 ^ (c + 1) - 1
          next = perfectList (c+1) n ls

prime :: (Integral a, Eq a, Ord a) => a -> Bool 
prime c | c <= 0     =  error "negative argument"
        | otherwise  =  primeD 2 c 

primeD :: (Integral a, Eq a, Ord a) => a -> a -> Bool            
primeD d c | d * d > c       =  True
           | c `mod` d == 0  =  False
           | otherwise       =  primeD (d+1) c 
