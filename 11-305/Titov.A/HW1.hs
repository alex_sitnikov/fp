module Task2
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b

-- Использовать fromIntegral для перевода из Integer в Double
-- задание заключалось в подсчете суммы ряда, а не в конвертации типа данных.
-- fromIntegral была как подсказка. (-1)
hw1_2 :: Integer -> Double
hw1_2 n = fromIntegral(n)

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
-- мудрёно, но сойдет. можно было просто шаг 2 сделать и считать обычный факториал
fact2 :: Integer -> Integer
fact2 n = 
	if n `mod` 2 == 0 then 
		(2 ^ (div n 2)) * (product [1.. (div n 2)])
	else
		div (product [1..n]) ((2^(div (n-1) 2)) * product [1.. (div (n-1) 2)])


       
-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
-- 1 же не простое.. (-0,5)
isPrime :: Integer -> Bool
isPrime 1 = True
isPrime 2 = True
isPrime n = isPrimeEratos n [2..n]

isPrimeEratos :: Integer -> [Integer] -> Bool --вернёт True если n осталось в хвосте списка после просеивания, 
--т. е. если оно простое
isPrimeEratos _ [] = False
isPrimeEratos n (x:[]) = n == x
isPrimeEratos n (x:xs) = if (last xsAfter) /= n
                                                   then False 
                                                   else isPrimeEratos n xs
                                                   where xsAfter = sift xs x n

-- чей то копипаст
sift :: [Integer] -> Integer -> Integer -> [Integer]
sift xs p n | p * p > n = xs
                              | otherwise = filter (\a -> a `mod` p /= 0) xs

-- сумма не верноая при подсчете от 1 (-0,5)
-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b = sum (filter (\x -> isPrime x) [a..b])

