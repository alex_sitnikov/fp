-- копипаст (см. Харисов)
module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 arg1 arg2 = (arg1 + arg2)
hw1_1 a b = error "hw1_1 is not implemented"

-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 n = fromIntegral (n)



-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n | n < 2 = 1
		| otherwise = n * fact2 (n - 2) 

fact2 n = error "fact2 not implemented yet"

-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isPrime :: Integer -> Bool
isPrime p | p == 1 = False
		  | otherwise = [i | i <- [2..round(sqrt(fromIntegral(p)))], p `mod` i == 0] == []
isPrime p = error "isPrime is not implemented yet"

-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b = sum(filter isPrime([a..b]))

primeSum a b = error "primeSum is not implemented yet"

