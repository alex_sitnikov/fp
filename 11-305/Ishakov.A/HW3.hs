-- хоть и не полный, но копипаст (см. Филиппов)
-- просрочено (/2)
module HW3
       ( root
       , sequence2_3_f
       ) where

import Prelude
import Data.Array
import Data.List
import Control.Applicative





f :: Double -> Double
f x = tan x + x - 1

rootHelp :: Double -> Double -> (Double -> Double) -> Double
rootHelp eps x fun = let y = fun x
                     in if abs(y - x) < eps then y
                                            else rootHelp eps y fun
root :: Double -> Double
root eps = rootHelp eps 1e0 f





factorialArray :: Integer -> Array Integer Integer
factorialArray n = a where a = array (0, n) ([(0,1), (1, 1)] ++ [(i, i * a!(i-1)) | i <-[2..n]])
-- почему до ста?? в априоре предпологалось, что последовательности бесконечные.
factorials = factorialArray 12
factorialsList = [factorials!x | x <-[0..12]]
squaresList = [x*x | x <- [0..100]]
cubesList = [x*x*x | x <- [0..100]]

finalList = sort (union (union squaresList cubesList) factorialsList)

sequence2_3_f :: Int -> Integer
sequence2_3_f a = finalList!!a





data Tree a = Empty | Node (Tree a) a (Tree a)

symmetricHelp :: Tree a -> Tree b -> Bool
symmetricHelp a b = case (a, b) of
                  (Empty, Empty)                     -> True
                  (Node t11 n1 t12, Node t21 n2 t22) -> (symmetricHelp t11 t21) && (symmetricHelp t12 t22)
                  (_, _)                             -> False
                
symmetric :: Tree a -> Bool
symmetric Empty          = True
symmetric (Node a1 a a2) = symmetricHelp a1 a2 





treeHelp  ::  (a -> b -> b) -> b -> Tree a -> b
treeHelp _ seed Empty           =  seed
treeHelp f seed (Node t1 n t2)  =  treeHelp f (f n (treeHelp f seed t2)) t1

splitting ::  Tree a -> [a]
splitting t  =  treeHelp (:) [] t

charHelp :: Char -> String -> Bool
charHelp = any . (==)

digitHelp :: String -> Bool
digitHelp s = let ds = map intToDigit [0..9]
                  in or $ (map (\c -> charHelp c s)) ds

listDigits :: Tree String -> [String]
listDigits tree = filter digitHelp (splitting tree)





data MultiTree a = Branch a [MultiTree a]

heapHelp :: (a -> Bool) -> Bool -> [MultiTree a] -> Bool
heapHelp p bol [] = bol
heapHelp p bol ((Branch b sub):bs) = heapHelp p ((&&) bol $ p b) bs

isHeap :: Ord a => MultiTree a -> Bool
isHeap (Branch b [])  = True
isHeap (Branch b sub) = if y then and (map isHeap sub) else False
                            where y = heapHelp (b<) True sub

