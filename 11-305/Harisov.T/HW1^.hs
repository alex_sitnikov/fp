module HW1 
( hw1_1 
, hw1_2 
, fact2 
, isPrime 
, primeSum 
) where 

-- |Вычислить сумму двух аргументов 
hw1_1 :: Integer -> Integer -> Integer 
hw1_1 a b = a + b 



-- |Вычислить сумму N членов ряда 
-- 
-- 
-- N 
-- --- 
-- \ 1 
-- > --- 
-- / k^k 
-- --- 
-- k=1 
-- 
-- Использовать fromIntegral для перевода из Integer в Double 
hw1_2 :: Integer -> Double 
hw1_2 n | n == 1 = 1 
		| otherwise = evaluate(n) + hw1_2(n - 1) 

evaluate :: Integer -> Double 
evaluate n = 1 / fromIntegral(n ^ n) 




-- |Вычислить двойной факториал 
-- n!! = 1*3*5*...*n, если n - нечетное 
-- n!! = 2*4*6*...*n, если n - четное 
-- на отрицательные должны ругаться. факториал определен только для неотрицательных чисел (ошибка).
fact2 :: Integer -> Integer 
fact2 n | n < 2 = 1 
		| otherwise = n * fact2 (n - 2) 




-- |Проверить заданное число на простоту 
-- Использовать div для целочисленного деления 
-- или mod для остатка от деления 
-- получше чем у Федорова, но 0 и отрицательные тоже простыми не являются.
isPrime :: Integer -> Bool 
isPrime p | p == 1 = False 
			| otherwise = [i | i <- [2..round(sqrt(fromIntegral(p)))], p `mod` i == 0] == [] 




-- |Найти сумму всех простых чисел в диапазоне [a;b] 
primeSum :: Integer -> Integer -> Integer 
primeSum a b = sum(filter isPrime([a..b]))