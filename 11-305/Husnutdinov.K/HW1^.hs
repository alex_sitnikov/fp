module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 x y = x + y
--hw1_1 x y = error "hw1_1 is not implemented"

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- |Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 n | n == 1 = 1 
         | otherwise = 1/(fromIntegral n ^ fromIntegral n) + hw1_2 (n-1)
--hw1_2 n = error "hw1_2 not implemented yet"

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n = error "fact2 not implemented yet"

-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
-- 1 не относится к простым (-0,5)
isPrime :: Integer -> Bool
isPrime n = all check [2..n `div` 2]
	where check x = n `mod` x /= 0
--isPrime n = error "isPrime is not implemented yet"

-- |Найти сумму всех простых чисел в диапазоне [a;b]
-- как следствие primeSum тоже учитывает 1. (-0,5)
isPrime' :: Integer -> Integer -> Bool
isPrime' n k | (k > (n `div` 2)) = True
            | (n `mod` k) == 0 = False
            | otherwise = isPrime' n (k + 1)
            
primeSum :: Integer -> Integer -> Integer
primeSum n m | (n > m) = primeSum m n
               | (n == m) = 0
-- требовалось посчитать сумму, а не количесво (-0,5)
--             | (isPrime' n 2) = n + primeSum (n+1) m
               | (isPrime' n 2) = 1 + primeSum (n+1) m
               | otherwise = primeSum (n+1) m       
 
--primeSum n k = error "primeSum is not implemented yet"

