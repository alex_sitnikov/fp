-- надо хотя бы компилить перед отправкой
module HW2
       ( list2_1
       , gcd
       , coPrime
       , gcfList
       , perfect
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Написать программу для нахождения минимального из
-- чисел, являющихся максимальными в каждой из строк заданной
-- прямоугольной матрицы.
-- нет реализации (-1)
list2_1 :: Integral a => [[a]] -> a
list2_1 n = error "list2_1 not implemented yet"

-- |Вычислить наибольший общий делитель(НОД) 2х натуральных чисел. Использовать алгоритм Евклида.
-- rem возвращает остаток от целочисленного деления первого аргумента на второй
gcd1 :: Integral a => a -> a -> a
gcd1 a b | (b > a) = gcd1 b a
          | ((a `rem` b) == 0) = b
          | otherwise = gcd1 b (a `rem` b)
--gcd a b = error "gcd not implemented yet"

-- |Проверить являются ли 2 натуральных числа взаимнопростыми.
-- 2 числа называются взаимнопростыми если их НОД равен 1
coPrime :: Integral a => a -> a -> a
coPrime n m | (m > n) = gcd m n 
         | (m == 0) = n
         | otherwise = gcd m (n `rem` m)
--coPrime n m = error "coPrime is not implemented yet"

-- |Напишите функцию нахождения самого маленького числа, которое делится
-- на все числа от одного до N.
-- Тут нок двух чисел:
-- с помощью gcd мы находим нод;
-- c помощью quot возвращаем значение целочисленного деления первого аргумента на второй
gcfList :: Integral a => a -> a -> a
gcfList _ 0 = 0
gcfList 0 _ = 0
gcfList m n = abs ((quot m (gcd m n)) * n)

-- |Написать программу для нахождения первых N совершенных чисел.
-- Совершенным числом называется натуральное число, равное
-- сумме всех своих делителей, включая единицу, но исключая само
-- это число. Так, например, число 28 – совершенное, поскольку
-- 28 = 1 + 2 + 4 + 7 + 14. 
perfect :: Integer -> [Integer]
-- собираем список делителей числа a
factors :: Integer -> [Integer]
factors a    = [ i | i<-[1..a-1], a `mod` i == 0 ]

-- определяем совершенное число
perfect' :: Integer -> Bool
perfect' a    = sum (factors a) == a

-- список совершенных чисел
perfects :: [Integer]
perfects     = filter perfect' [1..]
                                    -- встроенная функция filter выделяет из списка элементы с указанным свойством
									-- с помощью команды take 3 perfects можно увидеть 3 первых совершенных числа

-- согласно задаче стоило написать так.. (-0,5)
perfect n = take (fromIntegral n) perfects
--perfect a = error "perfect is not implemented yet"
