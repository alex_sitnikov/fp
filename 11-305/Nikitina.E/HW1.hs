-- см. Филиппова
module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = error "hw1_1 is not implemented"


-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 n | n < 0 = error "Cannot calculate summ of negative amount of range members!"
        | n == 0 = 0 
hw1_2 n = nsumm' n 0.0

--Вспомогательная функция для подсчёт суммы n членов ряда (с итератором)
nsumm' :: Integer -> Double -> Double
nsumm' 0 d = d
nsumm' n d = nsumm' (n - 1) (d + (nextRangeMember n))  --считает сумму в обратном порядке (начиная с n-го члена)

--Функция для получения k-го члена ряда
nextRangeMember :: Integer -> Double
nextRangeMember k = 1 / (fromIntegral k ^ 2)


-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n | n < 0 = error "Factorial is undefined for negatives!"
fact2 0 = 1
fact2 1 = 1
fact2 n = n * fact2 (n - 2)


-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isPrime :: Integer -> Bool
isPrime 1 = True
isPrime 2 = True
isPrime n = isPrimeEratos n [2..n]

--Определение простоты решетом Эратосфена
isPrimeEratos :: Integer -> [Integer] -> Bool   --вернёт True если n осталось в хвосте списка после просеивания, 
											    --т. е. если оно простое
isPrimeEratos _ [] = False
isPrimeEratos n (x:[]) = n == x
isPrimeEratos n (x:xs) = if (last xsAfter) /= n
                            then False 
							else isPrimeEratos n xs
	where xsAfter = sift xs x n
			  
--функция одной итерации просеивания
sift :: [Integer] -> Integer -> Integer -> [Integer]
sift xs p n | p * p > n = xs
            | otherwise = filter (\a -> a `mod` p /= 0) xs

-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b = sum (filter (\x -> isPrime x) [a..b]) --Суммирование элементов списка, отфильтрованного по простым числам

