-- �� ������ ���������� �������� ������ �������. �� ���� � ���� ������� �����, ���
-- �� � ���� �� ���������. ��� ��� 0, ��� � ������� 0
module HW4
   ( insertValue
   , slice
   , collectNodes
   , isEulerPathExist
   , paths
   ) where
    
-- |������� insertValue ������ ������ ��������� �������� � ������ �� ��������� 
-- �������. 
insertValue :: a -> [a] -> Int -> [a]
insertValue = error "insertValue is not implemented"

-- |������� slice ������ ���������� ��������� � �������� �� A �� B 
--		������	 �		B
slice :: [a] -> Int -> Int -> Maybe [a]
slice = error "slice is not implemented"

-- |�������� ������� collectNodes, ������� ��� ������� ������ ������ �������� ������
-- �������� ����� � ������� ����� ������ ���� �������.
data Tree a = Node a [Tree a]

collectNodes :: Tree a -> [[a]]
collectNodes = error "collectNodes is not implemented"

-- |������� isEulerPathExist ������ ��������� �������� �� ���� ������� ���� 
type Graph = [(Int, [Int])]

isEulerPathExist :: Graph -> Bool
isEulerPathExist = error "isEulerPathExist is not implemented"

-- |������� paths ������ ������� ������ ����� ����� 2�� ������� � �����.
-- ������ ���� �������������� ����� ������ � ��������� �� ����� � �� B.
-- ���� �������� ��� ������ ����� [(a,a)]. ��������, [(1,2),(2,3),(1,3),(3,4),(4,2),(5,6)]
--				 �	  B		����		
paths :: Eq a => a -> a -> [(a,a)] -> [a]
paths = error "paths is not implemented"