-- �������� (��. �������)
module HW1 
( hw1_1 
, hw1_2 
, fact2 
, isPrime 
, primeSum 
) where 

-- ��������� ��� ������: haskellstack.org 
-- stack setup - ��������� GHC ������ ������ � �.�. 
-- stack build - ���������� 
-- stack test - ����� 

-- |��������� ����� ���� ���������� 
hw1_1 :: Integer -> Integer -> Integer 
hw1_1 a b = a + b 

-- |��������� ����� N ������ ���� 
-- 
-- 
-- N 
-- --- 
-- \ 1 
-- > --- 
-- / k^k 
-- --- 
-- k=1 
-- 
-- ������������ fromIntegral ��� �������� �� Integer � Double 
hw1_2 :: Integer -> Double 
hw1_2 n 
	| n == 1 = 1 
	| otherwise = evaluate(n) + hw1_2(n - 1) 

evaluate :: Integer -> Double 
evaluate n = 1 / fromIntegral(n ^ n) 

-- |��������� ������� ��������� 
-- n!! = 1*3*5*...*n, ���� n - �������� 
-- n!! = 2*4*6*...*n, ���� n - ������ 
fact2 :: Integer -> Integer 
fact2 n 
	| n < 2 = 1 
	| otherwise = n * fact2 (n - 2) 

-- |��������� �������� ����� �� �������� 
-- ������������ div ��� �������������� ������� 
-- ��� mod ��� ������� �� ������� 
isPrime :: Integer -> Bool 
isPrime p 
	| p == 1 = False 
	| otherwise = [i | i <- [2..round(sqrt(fromIntegral(p)))], p `mod` i == 0] == [] 

-- |����� ����� ���� ������� ����� � ��������� [a;b] 
primeSum :: Integer -> Integer -> Integer 
primeSum a b = sum(filter isPrime([a..b]))