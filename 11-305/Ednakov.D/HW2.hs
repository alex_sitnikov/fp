-- копипаст (см. Федоров) 
-- просрочено (/2)
module HW2
       ( list2_1
       , gcd_1
       , coPrime
       , gcfList
       , perfect
       ) where

						-- Студент Еднаков Даниил, группа 11-305 --

-- |Написать программу для нахождения минимального из
-- чисел, являющихся максимальными в каждой из строк заданной
-- прямоугольной матрицы.
list2_1 :: Integral a => [[a]] -> a
list2_1 n = minimum (maxes n)

maxes ::Integral a => [[a]] -> [a]
maxes n = map (maximum)n

-- |Вычислить наибольший общий делитель(НОД) 2х натуральных чисел. Использовать алгоритм Евклида.
gcd_1 :: Integral a => a -> a -> a
gcd_1 a b | a < b = gcd b a
        | b < 0 = error "Can't be negative"
gcd_1 a 0         = a
gcd_1 a b         = gcd_1 b (a `mod` b)

-- |Проверить являются ли 2 натуральных числа взаимнопростыми.
-- 2 числа называются взаимнопростыми если их НОД равен 1
coPrime :: Integral a => a -> a -> Bool
coPrime a b = if gcd_1 a b == 1 then True
                                else False

-- |Напишите функцию нахождения самого маленького числа, которое делится
-- на все числа от одного до N.
gcfList :: (Num a, Integral a) => [a] -> a
gcfList (x:ls) = lcm_1 x ls

-- least common multiple of list
lcm_1 :: (Num a, Integral a) => a -> [a] -> a
lcm_1 list[] = list
lcm_1 list(x:ls) = lcm_1(list * x `div` (gcd_1 list x)) ls

-- |Написать программу для нахождения первых N совершенных чисел.
-- Совершенным числом называется натуральное число, равное
-- сумме всех своих делителей, включая единицу, но исключая само
-- это число. Так, например, число 28 – совершенное, поскольку
-- 28 = 1 + 2 + 4 + 7 + 14.
perfect :: Integral a => a -> [a]
perfect n = [x | x <- [1..n], sum_of_dividers x == x]

sum_of_dividers :: Integral a => a -> a
sum_of_dividers n = sum [x | x <- [1..n], n `mod` x == 0, x /= n]