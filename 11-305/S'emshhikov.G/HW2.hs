-- просрочено (/4)
module TASK2
       ( list2_1
       , gcd
       , coPrime
       , gcfList
       ) where


list2_1 :: Integral a => [[a]] -> a
list2_1 [] = error "list2_1: empty argument"
list2_1 arr = minimum [maximum x| x <- arr]

-- дергаете функцию из стандартной библиотеки(-0,5)
gcd' :: Integral a => a -> a -> a
gcd' a b 
	|a <= 0 || b < 0 = error "gcd: non-positive argument"
 	|b == 0 = a
  	|otherwise = gcd' b (mod a b)

coPrime :: Integral a => a -> a -> Bool
coPrime a b |gcd' a b == 1 = True
	    |otherwise = False

gcfList :: Integral a => [a] -> a
gcfList arr = foldr (\x y -> x * y `div` gcd' x y) 1 arr