-- просрочено (/2)
module TASK4
   ( insertValue
   , slice
   ) where


insertValue :: a -> [a] -> Int -> [a]
insertValue y xs n = countdown n xs where
   countdown 0 xs = y:countdown n xs 
   countdown _ [] = []
   countdown m (x:xs) = x:countdown (m-1) xs

-- drop не возвращает Maybe. надо было написать обертку(-0,5)
slice :: [a] -> Int -> Int -> Maybe [a]
slice xs from to  = take (to - from + 1) (drop from xs)