-- просрочено (/2)
module TASK1
       ( hw1
       , hw2
       , hw3
       , hw4
       ) where

hw1 :: Integer -> Integer -> Integer
hw1 x y = x + y


hw2 :: Integer -> Double
hw2' :: Integer -> Double -> Double
hw2 n
	| n > 0 = hw2' n 0.0
hw2' n res 
	| n == 0 = res
	| n > 0 = hw2' (n - 1) (res + 1 / fromIntegral (n*n))


hw3 :: Integer -> Integer
hw3' :: Integer -> Integer -> Integer
hw3 n  
	| n >= 0 = hw3' n 1
hw3' n res
	| n <= 1 = res
	| n > 1 = hw3' (n - 2) (res * n)


hw4 :: Integer -> Bool
hw4' :: Integer -> Integer -> Bool
hw4 n 
	| n > 0 = hw4' n 2
hw4' n d
	| d * d > n = True 
	| n `mod` d == 0 = False
	| otherwise = hw4' n (d + 1)