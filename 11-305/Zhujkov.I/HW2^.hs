module HW2
       ( list2_1
       , gcdHW
       , coPrime
       , gcfList
       --, perfect
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Написать программу для нахождения минимального из
-- чисел, являющихся максимальными в каждой из строк заданной
-- прямоугольной матрицы.
list2_1 :: Integral a => [[a]] -> a
list2_1 n = minimum(linemax n)

--Нахождение максимума в строке матрицы
linemax :: Integral  a => [[a]] -> [a]
linemax x     =  map (maximum) x


-- |Вычислить наибольший общий делитель(НОД) 2х натуральных чисел. Использовать алгоритм Евклида.
gcdHW :: Integral a => a -> a -> a
gcdHW a b | c ==0  = b
          | otherwise = gcdHW b c 
 where c = a `mod` b


-- |Проверить являются ли 2 натуральных числа взаимнопростыми.
-- 2 числа называются взаимнопростыми если их НОД равен 1
coPrime :: Integral a => a -> a -> Bool
coPrime a b | gcdHW a b == 1 = True
            |otherwise       = False

-- |Напишите функцию нахождения самого маленького числа, которое делится
-- на все числа от одного до N.
gcfList :: (Num a,Integral a) => [a] -> a
gcfList (x:ls) = gcfLM x ls


gcfLM :: (Num a,Integral a) => a -> [a] -> a
gcfLM a [] = a
gcfLM a (x:y) = gcfLM (a*(x `div` (gcdHW a x))) y

{-
-- |Написать программу для нахождения первых N совершенных чисел.
-- Совершенным числом называется натуральное число, равное
-- сумме всех своих делителей, включая единицу, но исключая само
-- это число. Так, например, число 28 – совершенное, поскольку
-- 28 = 1 + 2 + 4 + 7 + 14. 
perfect :: Integer a => a -> [a]
perfect a = error "perfect is not implemented yet"
-}
