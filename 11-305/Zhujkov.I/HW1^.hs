fib        :: Integer -> Integer
fib 1      =  1
fib 2      =  1
fib n      =  fib (n-1) + fib (n-2)

add :: Integer -> Integer -> Integer
add a b = a+b

square :: Integer -> Integer
square a = a*a

row :: Integer -> Integer
row 0 = 0
row 1 = 1
row n 		= row(n-1) + square(n)

conv :: Integer -> Double
conv a = fromIntegral(a)


