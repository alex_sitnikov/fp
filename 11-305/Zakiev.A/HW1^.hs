-----------------------------------------------------------------------------
--
-- Module      :  Main
-- Copyright   :
-- License     :  AllRightsReserved
--
-- Maintainer  :
-- Stability   :
-- Portability :
--
-- |
--
-----------------------------------------------------------------------------

module Main (
    main
) where

-- fibonacci function
fibonacci :: Int -> Int
fibonacci 1=1
fibonacci 2=1
fibonacci n=fibonacci(n-1)+fibonacci(n-2)

--sum of two Integers
sumOfTwo :: Integer -> Integer -> Integer
sumOfTwo a b = a + b

-- сумма ряда отсутсвует (-1)

--double factorial
doubleFactorial :: Integer -> Integer
doubleFactorial n | n < 0 = error "Don't use negatives, pls."
doubleFactorial 0 = 1
doubleFactorial 1 = 1
doubleFactorial n = n * doubleFactorial (n - 2)

--primarity check
-- 1 - не простое, а 2 - простое (-0,5)
isPrime :: Integer -> Bool
isPrime p | p<0 = error "Negatives shall not pass!"
isPrime p | p `mod` 2==0 = False
isPrime p | otherwise = isPrime' p 3

isPrime' p x|p<x*x = True
isPrime' p x|p `mod` x ==0  = False
isPrime' p x|otherwise = isPrime' p (x+2)

--prime numbers sum
-- сумма от 1 или 2 не правильная (-0,5)
primeSum :: Integer -> Integer -> Integer
primeSum a b = sum (filter (\x -> isPrime x) [a..b])

-- main function to launch
main :: IO ()
main = print(primeSum 0 11)