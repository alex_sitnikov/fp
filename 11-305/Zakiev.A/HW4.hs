-- пустовато..
module HW4
   ( main
   ) where
    
-- |Функция insertValue должна должна вставлять значение в список на указанную 
-- позицию. 
insertValue :: a -> [a] -> Int -> [a]
insertValue = error "insertValue is not implemented"

-- |Функция slice должна возвращать подсписок в пределах от A до B 
--		список	 А		B
slice :: [a] -> Int -> Int -> Maybe [a]
slice = error "slice is not implemented"

-- |Написать функцию collectNodes, которая для каждого уровня дерева составит список
-- значений узлов и выведет общий список всех уровней.
data Tree a = Node a [Tree a]

collectNodes :: Tree a -> [[a]]
collectNodes = error "collectNodes is not implemented"

-- |Функция isEulerPathExist должна проверить содержит ли граф Эйлеров путь 
type Graph = [(Int, [Int])]

isEulerPathExist :: Graph -> Bool
isEulerPathExist = error "isEulerPathExist is not implemented"

-- |Функция paths должна вернуть список путей между 2мя точками в графе.
-- Каждый путь представляется собой список с вершинами от точки А до B.
-- Граф задается как список ребер [(a,a)]. Например, [(1,2),(2,3),(1,3),(3,4),(4,2),(5,6)]
--				 А	  B		граф		
paths :: Eq a => a -> a -> [(a,a)] -> [a]
paths = error "paths is not implemented"