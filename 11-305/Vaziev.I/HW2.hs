-- копипаст (см. Федоров) 
-- просрочено, при чем дважды (/4) 
module HW2
( list2_1
, gcd_1
, coPrime
, gcfList
, perfect
) where

maxes ::Integral a => [[a]] -> [a]
maxes n = map (maximum)n

list2_1 :: Integral a => [[a]] -> a
list2_1 n = minimum (maxes n)



gcd_1 :: Integral a => a -> a -> a
gcd_1 a b | a < b = gcd b a
          | b < 0 = error "Can't be negative"
gcd_1 a 0 = a
gcd_1 a b = gcd_1 b (a `mod` b)



coPrime :: Integral a => a -> a -> Bool
coPrime a b = if gcd_1 a b == 1 then True
else False



gcfHelp :: (Num a, Integral a) => a -> [a] -> a
gcfHelp list[] = list
gcfHelp list(x:ls) = gcfHelp(list * x `div` (gcd_1 list x)) ls

gcfList :: (Num a, Integral a) => [a] -> a
gcfList (x:ls) = gcfHelp x ls



perfectHelp :: Integral a => a -> a
perfectHelp n = sum [x | x <- [1..n], n `mod` x == 0, x /= n]

perfect :: Integral a => a -> [a]
perfect n = [x | x <- [1..n], perfectHelp x == x]

