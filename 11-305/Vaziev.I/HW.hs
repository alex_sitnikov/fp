-- копипаст (см. Исхаков)
-- просрочено (/2)
-- весь семестр не было инета, что вы задержали все домашки от первой
-- до последней? ни за что не поверю.. 
module HW
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       , fibonacciByLimit
       , fibonacciByCount
       ) where

hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b

hw1_2 :: Integer -> Double
hw1_2 n = hw1_2help (fromIntegral 1)  (fromIntegral n)

hw1_2help :: Double -> Double -> Double
hw1_2help a b |a == b = 1/(a**a)
              |a < b = 1/(a**a) + hw1_2help (a + 1) b 

fact2 :: Integer -> Integer
fact2 n |n <= 2 = n
        |n > 2 = n * fact2 (n - 2)

isPrime :: Integer -> Bool
isPrime p = isPrimeHelp (p - 1) p

isPrimeHelp :: Integer -> Integer -> Bool
isPrimeHelp a b |a == 2 = b `mod` a /= 0
                |a > 2 = b `mod` a /= 0 && isPrimeHelp (a - 1) b

primeSum :: Integer -> Integer -> Integer
primeSum a b |a == b && isPrime a = a
             |a == b = 0
             |isPrime a = a + primeSum (a + 1) b
             |isPrime a == False = primeSum (a + 1) b

fibonacciByLimit :: Integer -> String
fibonacciByLimit n = fibLim 0 1 n

fibLim :: Integer -> Integer -> Integer -> String
fibLim a b n |b > n = show a
          |b <= n = show a ++ " " ++ fibLim b (a + b) n

fibonacciByCount :: Integer -> Integer
fibonacciByCount 0 = 0
fibonacciByCount 1 = 1
fibonacciByCount n = fibonacciByCount (n - 1) + fibonacciByCount (n - 2)
