module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a+b;

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
-- проверки на отрицательные нет (-0,5)
hw1_2 :: Integer -> Double
hw1_2' :: Integer -> Double -> Double
hw1_2 n = hw1_2' n 0.0
hw1_2' 0 ans = ans
hw1_2' n ans = hw1_2' (n-1) (ans+1/fromIntegral(n*n))

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n = if odd n then foldr (*) 1 [1,3..n] else foldr (*) 1 [2,4..n]

-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
-- 1 не простое (-0,5)
isPrime :: Integer -> Bool
isPrime' :: Integer -> Integer -> Bool
isPrime n = isPrime' n 2
isPrime' n d
	| d*d > n = True 
	| n `mod` d == 0 = False
	| otherwise = isPrime' n (d+1)

-- |Найти сумму всех простых чисел в диапазоне [a;b]
-- сумма от 1 не правильна (-0,5)
primeSum :: Integer -> Integer -> Integer
primeSum a b = foldr (+) 0 [x | x <- [a..b], isPrime x]
