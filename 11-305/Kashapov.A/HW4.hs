-- �������� (��. ��������)
module HW4
   ( insertValue
   , slice
   , collectNodes
   , isEulerPathExist
   , paths
   ) where
   
import Prelude
import Control.Applicative

-- |������� insertValue ������ ������ ��������� �������� � ������ �� ��������� 
-- �������. 
insertValue :: a -> [a] -> Int -> [a]
insertValue e ls 0 = (e:ls)
insertValue e [] n = []
insertValue e (l:ls) n = l : (insertValue e ls (n-1))

-- |������� slice ������ ���������� ��������� � �������� �� A �� B 
--		������	 �		B
slice :: [a] -> Int -> Int -> Maybe [a]
slice ls a b | (a < 0) || (b < a) = Nothing
slice ls a b = sequence (slice' ls a b)

slice' :: [a] -> Int -> Int -> [Maybe a]
slice' [] a b | a > 0  = [Nothing]
              | True = []
slice' (l:ls) a b | b == -1 = []
                  | a == 0 = Just l : (slice' ls 0 (b-1)) --if a==0 then we begin to put elemnts to result list
                  | True   = slice' ls (a-1) (b-1)        --otherwise skip those elements


data Tree a = Node a [Tree a]

getNode :: Tree a -> a
getNode (Node a tr) = a

getTrees :: Tree a -> [Tree a]
getTrees (Node a tr) = tr

-- |�������� ������� collectNodes, ������� ��� ������� ������ ������ �������� ������
-- �������� ����� � ������� ����� ������ ���� �������.
collectNodes :: Tree t -> [[t]]
collectNodes (Node t tr) = ([t] :) $ collectNodes' ((Node t tr):[])

collectNodes' :: [Tree t] -> [[t]]
collectNodes' tr = let childTrees = concat (map getTrees tr)
                   in if null childTrees then [] else (map getNode childTrees) : (collectNodes' childTrees)


-- |������� isEulerPathExist ������ ��������� �������� �� ���� ������� ���� 
type Graph = [(Int, [Int])]

isEulerPathExist :: Graph -> Bool
isEulerPathExist cs = l == 0 || l == 2 
                        where l = length (filter ((/= 0) . (mod 2) . length . snd) cs)

-- |������� paths ������ ������� ������ ����� ����� 2�� ������� � �����.
-- ������ ���� �������������� ����� ������ � ��������� �� ����� � �� B.
-- ���� �������� ��� ������ ����� [(a,a)]. ��������, [(1,2),(2,3),(1,3),(3,4),(4,2),(5,6)]
--				 �	  B		����		
-- �������� (��. �����������)
paths :: Eq a => a -> a -> [(a,a)] -> [a]
paths cm start goal = paths' [] cm start goal
	where paths' visited cm start goal
		| start == goal 	  = [[start]]
		| start `notElem` visited = [start:rest | next <- neighbours cm start,
												  rest <- paths' (start:visited) cm next goal]
		| otherwise 		  = []