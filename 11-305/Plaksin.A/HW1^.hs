module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Фибоначи
f :: Integer -> Integer
f 0 = 0
f 1 = 1
f n = f (n-1) + f (n-2)

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b
-- |Вычислить сумму N членов ряда
hw1_1_1 n = foldr (+) 0 [1 / fromIntegral(k^k) | k <- [1..n]]
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 n = fromIntegral(n)

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n | n < 0 = error "Data is incorrect"
fact2 0 = 1
fact2 1 = 1
fact2 n = n * fact2 (n - 2)


-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
-- 1 не простое (-0,5)
isPrime :: Integer->Bool
isPrime x = null [y | y <- [2..floor (sqrt (fromIntegral x))], x `mod` y == 0]

-- |Найти сумму всех простых чисел в диапазоне [a;b]
-- сумма от 1 не правильная (-0,5)
primeSum :: Integer -> Integer -> Integer
primeSum a b = sum (filter (\x -> isPrime x) [a..b])