-- �������� (��. ��������)
module HW4
   ( insertValue
   , slice
   , collectNodes
   , isEulerPathExist
   , paths
   ) where
import Prelude
import Control.Applicative

-- |������� insertValue ������ ������ ��������� �������� � ������ �� ��������� �������. 
insertValue :: a -> [a] -> Int -> [a]
insertValue e ls 0 = (e:ls)
insertValue e [] n = []
insertValue e (l:ls) n = l : (insertValue e ls (n-1))

-- |������� slice ������ ���������� ��������� � �������� �� A �� B 
slice :: [a] -> Int -> Int -> Maybe [a]
slice ls a b | (a < 0) || (b < a) = Nothing
slice ls a b = sequence (slice' ls a b)

slice' :: [a] -> Int -> Int -> [Maybe a]
slice' [] a b | a > 0  = [Nothing]
              | True = []
slice' (l:ls) a b | b == -1 = []
                  | a == 0 = Just l : (slice' ls 0 (b-1)) --if a==0
                  | True   = slice' ls (a-1) (b-1) 

data Tree a = Node a [Tree a]

getNode :: Tree a -> a
getNode (Node a tr) = a

getTrees :: Tree a -> [Tree a]
getTrees (Node a tr) = tr

-- |�������� ������� collectNodes, ������� ��� ������� ������ ������ �������� ������
-- �������� ����� � ������� ����� ������ ���� �������.
collectNodes :: Tree a -> [[a]]
collectNodes (Node a tr) = ([a] :) $ collectNodes' ((Node a tr):[])

collectNodes' :: [Tree a] -> [[a]]
collectNodes' tr = let childTrees = concat (map getTrees tr)
                   in if null childTrees then [] else (map getNode childTrees) : (collectNodes' childTrees)

-- |������� paths ������ ������� ������ ����� ����� 2-�� ������� � �����.
-- ������ ���� �������������� ����� ������ � ��������� �� ����� � �� B.
-- ���� �������� ��� ������ ����� [(a,a)]. ��������, [(1,2),(2,3),(1,3),(3,4),(4,2),(5,6)]		
paths :: Eq a => a -> a -> [(a,a)] -> [[a]]
paths = buildPaths []

buildPaths :: Eq a => [a] -> a -> a -> [(a, a)] -> [[a]]
buildPaths builded st fin _ | (st == fin) = (fin:builded) : []
buildPaths builded st fin g = let stack = map (\s -> fst s) $ filter (\x -> (snd x) == fin && elem (fst x) builded == False) g
                              in if   (null stack) 
								 then []
								 else (concatMap (\s -> buildPaths (fin:builded) st s g) stack)

-- |������� isEulerPathExist ������ ��������� �������� �� ���� ������� ���� 
type Graph = [(Int, [Int])]

isEulerPathExist :: Graph -> Bool
isEulerPathExist cs = w == 0 || w == 2 
                        where w = length (filter ((/= 0) . (mod 2) . length . snd) cs)