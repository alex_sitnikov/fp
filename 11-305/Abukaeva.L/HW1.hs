-- ������ �� �������� � ���������
module H1
       ( hw1_1
       , hw1_2
       , isPrime
       , primeSum
       , fib
       , fact2
       ) where

-- |��������� ����� ���� ����������
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b

-- |��������� ����� N ������ ����
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- ������������ fromIntegral ��� �������� �� Integer � Double
hw1_2 :: Integer -> Double
hw1_2 n | n < 0 = error "Cannot calculate summ of negative amount of range members!"
        | n == 0 = 0 
hw1_2 n = nsumm' n 0.0

--��������������� ������� ��� ������� ����� n ������ ���� (� ����������)
nsumm' :: Integer -> Double -> Double
nsumm' 0 d = d
nsumm' n d = nsumm' (n - 1) (d + (nextRangeMember n))  --������� ����� � �������� ������� (������� � n-�� �����)

--������� ��� ��������� k-�� ����� ����
nextRangeMember :: Integer -> Double
nextRangeMember k = 1 / (fromIntegral k ^ 2)

--���������
fib :: Integer -> Integer
fib 1 =  1
fib 2 =  1
fib n =  fib (n-1) + fib (n-2)

-- |��������� �������� ����� �� ��������
-- ������������ div ��� �������������� ������� ��� mod ��� ������� �� �������
isPrime :: Integer -> Bool
isPrime 1 = True
isPrime 2 = True
isPrime n = isPrimeEratos n [2..n]

--����������� �������� ������� ����������
isPrimeEratos :: Integer -> [Integer] -> Bool
isPrimeEratos _ [] = False
isPrimeEratos n (x:[]) = n == x
isPrimeEratos n (x:xs) = if (last xsAfter) /= n
                            then False 
							else isPrimeEratos n xs
	where xsAfter = sift xs x n
			  
sift :: [Integer] -> Integer -> Integer -> [Integer]
sift xs p n | p * p > n = xs
            | otherwise = filter (\a -> a `mod` p /= 0) xs

-- |����� ����� ���� ������� ����� � ��������� [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b = sum (filter (\x -> isPrime x) [a..b]) 

-- |��������� ������� ���������
-- n!! = 1*3*5*...*n, ���� n - ��������
-- n!! = 2*4*6*...*n, ���� n - ������
fact2 :: Integer -> Integer
fact2 n | n < 0 = error "Factorial is undefined for negatives"
fact2 0 = 1
fact2 1 = 1
fact2 n = n * fact2 (n - 2)


