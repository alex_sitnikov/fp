module HW5_1
   ( rotateRight
   , sortList
   , multiPrimeFactor
   , sphenicList
   , calc
   ) where

import Prelude
import Data.Maybe
import Control.Applicative
import Data.List
   
-- |������� rotateRight ��������� ������ ������������ ������ � ������������� ����� n
-- ����� ���� ���������� ����������� �������� ������� n ���. [a, b, c, d, e] -> [e, a, b, c, d] 
--						n
rotateRight :: [a] -> Int -> [a]
rotateRight [] _ = []
rotateRight ls n = let l = length ls
                   in take l . drop (l-n) . cycle $ ls

-- |������� sortList ��������� ��������� ������ �� ����� ��� ����������.
sortList :: [[a]] -> [[a]]
sortList [] = []
sortList [l] = [l]
sortList (l:ls) = sortList (filter (\a -> length a < length l) ls) ++ [l] ++ sortList (filter (\a -> length a >= length l) ls)

-- |������� multiPrimeFactor �� ��������� ����� ����� ������ �� ��� ������� ���������
-- � ���������� �� ���������. ������ ����������� � ������� ����������� ������� ���������.
--							prime,count
multiPrimeFactor :: Int -> [(Int,Int)]
multiPrimeFactor n = countDivs n (getDivisors n)

countDivs :: Int -> [Int] -> [(Int,Int)]
countDivs n divs = zip divs (map (\d -> countPrime n d) divs)

--          number divisor count
countPrime :: Int -> Int -> Int
countPrime n d | (n `mod` d) == 0 = 1 + (countPrime (n `div` d) d)
               | otherwise        = 0

-- |������� sphenicList ���������� ������ ����������� ����� � ���������� �� � �� �.
-- ru.wikipedia.org/wiki/�����������_�����
--				�		�
sphenicList :: Int -> Int -> [Int]
sphenicList a b = let ps = allPrimeNumbers b
                  in (let res = mk a b ps (tail ps) (tail $ tail ps)
                      in sort res)

--sort :: Ord a => [a] -> [a]                      
--sort (l:ls) = sort (filter (< l) ls) ++ [l] ++ sort (filter (>= l) ls)

mk :: Int -> Int -> [Int] -> [Int] -> [Int] -> [Int]
mk a b (p1:ls1) (p2:ls2) (p3:ls3) | b1 < p1 = []
                                  | b1 < p2 = mk a b ls1 (tail ls1) (tail $ tail ls1)
                                  | b1 `div` p2 < p2 = mk a b ls1 (tail ls1) (tail $ tail ls1)
                                  | a `div` p1 `div` p2 > p3 = mk a b (p1:ls1) (p2:ls2) ls3
                                  | True    = mk1 a b (p1:ls1) (p2:ls2) (p3:ls3)
                                      where b1 = b `div` p1

mk1 :: Int -> Int -> [Int] -> [Int] -> [Int] -> [Int]                                      
mk1 a b (p1:ls1) (p2:ls2) (p3:ls3) | b `div` p1 `div` p2 >= p3 = let r=(p1*p2*p3) 
                                                                 in if r >= a 
                                                                    then r:(mk1 a b (p1:ls1) (p2:ls2) ls3)
                                                                    else (mk1 a b (p1:ls1) (p2:ls2) ls3)
                                   | otherwise                 = mk a b (p1:ls1) ls2 (tail ls2)
                                      
-- |�������  ������������ �������� ������������� ��������� ����������� � ���������� �����.
-- ������, ["one","two","one","+","six","*","two","six"] -> (121 + 6) * 26 = 3302
-- ������������ ���������: +, -, *, /, % (������� �� ������) , ^ (������� �����)
-- ���������� �������� ���, ������ ��� ���������� ������������ ����� ������� ���������������.
calc :: [String] -> Int
calc ls = calc' ls Nothing Nothing Nothing

--          ls         first             operation              second      
calc' :: [String] -> Maybe Int -> Maybe (Int -> Int -> Int) -> Maybe Int -> Int
calc' [] Nothing Nothing Nothing = 0
calc' [] (Just f) Nothing Nothing = f
calc' [] (Just f) (Just op) Nothing = error "The expression must be ended with some number!"
calc' [] (Just f) (Just op) (Just s) = op f s
							
calc' (l:ls) a o b = let n0 = toNum l
                     in case (a, o, b, n0) of 
                         (Nothing, Nothing, Nothing, Nothing)     -> error "The expression must start from digit!"
                         (Nothing, Nothing, Nothing, n)           -> calc' ls n Nothing Nothing
                         (f0, Nothing, Nothing, Nothing)          -> calc' ls f0 (toOp l) Nothing
                         ((Just f), Nothing, Nothing, (Just n))   -> calc' ls (Just (f*10+n)) Nothing Nothing
                         (f0, op, Nothing, Nothing)               -> error "There are may not be two operation symbols running!"
                         (f0, op, Nothing, n)                     -> calc' ls f0 op n
                         ((Just f), (Just op), (Just s), Nothing) -> calc' ls (Just (op f s)) (toOp l) Nothing
                         (f0, op, (Just s), (Just n))             -> calc' ls f0 op (Just (s*10+n))
						   
toNum s = case s of
             "zero"  -> Just 0
             "one"   -> Just 1
             "two"   -> Just 2
             "three" -> Just 3
             "four"  -> Just 4
             "five"  -> Just 5
             "six"   -> Just 6
             "seven" -> Just 7
             "eight" -> Just 8
             "nine"  -> Just 9
             _       -> Nothing

toOp s = case s of
           "+" -> Just (+)
           "-" -> Just (-)
           "*" -> Just (*)
           "/" -> Just div
           "%" -> Just mod
           "^" -> Just (^)
           _   -> error "Invalid symbol: symbol is undefined for this expression!"

isPrime num
  | num <= 1 = False
  | otherwise = getDivisors num == [num]

getDivisors :: Int -> [Int]
getDivisors n = let r = [x | x <- [2..(n `div` 2)], (n `mod` x == 0) && (isPrime x)]
                in if r == [] then [n] else r
isqrt = floor . sqrt . fromIntegral  -- (sqrt Int) -> Int
  
allPrimeNumbers n = [2] ++ filter isPrime [3,5..(n `div` 2)] 