module HW2
       ( list2_1
       , gcdHW2
       , coPrime
       , gcfList
       , perfect
       ) where
-- � ���� Prelude, ���� ��� ��� ���� ����������
-- import Prelude
       
-- ��������� ��� ������: haskellstack.org
-- stack setup - ��������� GHC ������ ������ � �.�.
-- stack build - ����������
-- stack test  - �����

-- |�������� ��������� ��� ���������� ������������ ��
-- �����, ���������� ������������� � ������ �� ����� ��������
-- ������������� �������.
list2_1 :: Integral a => [[a]] -> a
list2_1 n = minimum (mapToMaxes n)

-- ������� ������ ��������� � ��������� ������ �������
mapToMaxes :: Integral a => [[a]] -> [a]
mapToMaxes n = map (maximum) n

-- |��������� ���������� ����� ��������(���) 2� ����������� �����. ������������ �������� �������.
gcdHW2 :: Integral a => a -> a -> a
gcdHW2 a b | c == 0     = b
           | otherwise  = gcdHW2 b c
    where c = a `mod` b

-- |��������� �������� �� 2 ����������� ����� ���������������.
-- 2 ����� ���������� ��������������� ���� �� ��� ����� 1
coPrime :: Integral a => a -> a -> Bool
coPrime a b | gcdHW2 a b == 1  = True
            | otherwise        = False

-- |�������� ������� ���������� ������ ���������� �����, ������� �������
-- �� ��� ����� �� ������ �� N.
gcfList :: (Num a, Integral a) => [a] -> a
gcfList (x:xs) = gcfList' x xs

-- �������, ������� ��������� ��� ����� �� ������
gcfList' :: (Num a, Integral a) => a -> [a] -> a
gcfList' e [] = e
gcfList' e (x:xs) = gcfList' (e * x `div` (gcdHW2 e x)) xs

-- |�������� ��������� ��� ���������� ������ N ����������� �����.
-- ����������� ������ ���������� ����������� �����, ������
-- ����� ���� ����� ���������, ������� �������, �� �������� ����
-- ��� �����. ���, ��������, ����� 28 � �����������, ���������
-- 28 = 1 + 2 + 4 + 7 + 14. 
perfect :: Integer -> [Integer]
perfect a = perfect' 1 a [] 1

-- �������, ����������� ������ ����������� �����. 
-- counter - ������� - �������� �� ����� �������� (����� ������)
perfect' :: Integer -> Integer -> [Integer] -> Integer -> [Integer]
perfect' p n ls counter | counter > n = ls
                        | prime p2    = p2 * ((p2 + 1) `div` 2) : (next (counter+1)) 
                        | otherwise   = next counter
    where p2 = 2 ^ (p + 1) - 1
          next = perfect' (p+1) n ls
                
-- ������� ��������, �������� �� �����-�������� �������.
prime :: (Integral a, Eq a, Ord a) => a -> Bool 
prime p | p <= 0     =  error "Non-positive argument!"
        | otherwise  =  prime' 2 p 

-- ��������������� ������� ��� �������� �� ��������, ������������ ��������.
prime' :: (Integral a, Eq a, Ord a) => a -> a -> Bool            
prime' d p | d * d > p       =  True
           | p `mod` d == 0  =  False
           | otherwise       =  prime' (d+1) p 
