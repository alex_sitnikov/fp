module HW2
       ( list2_1
       , gcdHW2
       , coPrime
       , gcfList
      , perfect
       ) where

-- |�������� ��������� ��� ���������� ������������ ��
-- �����, ���������� ������������� � ������ �� ����� ��������
-- ������������� �������.
list2_1 :: Integral a => [[a]] -> a
list2_1 n = minimum (mapToMaxes n)

-- ������� ��������� ����� ��������� � ��������� ������ �������
mapToMaxes :: Integral a => [[a]] -> [a]
mapToMaxes n = map (maximum) n

-- |��������� ���������� ����� ��������(���) 2� ����������� �����. ������������ �������� �������.
gcdHW2 :: (Integral a) => a -> a -> a
gcdHW2 a 0 = a
gcdHW2 a b = gcdHW2 b (a `mod` b)

-- |��������� �������� �� 2 ����������� ����� ���������������.
-- 2 ����� ���������� ��������������� ���� �� ��� ����� 1
coPrime :: Integral a => a -> a -> Bool
coPrime a b | gcdHW2 a b == 1  = True
            | otherwise        = False
         
-- |�������� ������� ���������� ������ ���������� �����, ������� �������
-- �� ��� ����� �� ������ �� N.
gcfList :: (Num a, Integral a) => [a] -> a
gcfList (x:ls) = gcfList' x ls

-- �������, ������� ��������� ���������� ����� ������� ����� �� ������
gcfList' :: (Num a, Integral a) => a -> [a] -> a
gcfList' e [] = e
gcfList' e (x:ls) = gcfList' (e * x `div` (gcdHW2 e x)) ls

-- |�������� ��������� ��� ���������� ������ N ����������� �����.
-- ����������� ������ ���������� ����������� �����, ������
-- ����� ���� ����� ���������, ������� �������, �� �������� ����
-- ��� �����. ���, ��������, ����� 28 � �����������, ���������
-- 28 = 1 + 2 + 4 + 7 + 14. 

--������� ������� ��� "take N perfect"
--������ ������� perfect n = take (fromIntegral n) perfect' (-0,5)

perfect :: [Integer]
perfect = [p n | n <- primes, isPrime (m n)]
        where p n = 2^(n - 1) * (m n)
              m n = 2^n - 1

primes = [n | n <- [1..], isPrime n]
         where isPrime x = (divisors x == [1])

isPrime :: (Integral a, Eq a, Ord a) => a -> Bool 
isPrime p | p <= 0     =  error "Non-positive argument!"
        | otherwise  =  prime' 2 p 

-- ��������������� ������� ��� �������� �� ��������, ������������ ��������.
prime' :: (Integral a, Eq a, Ord a) => a -> a -> Bool            
prime' d p | d * d > p       =  True
           | p `mod` d == 0  =  False
           | otherwise       =  prime' (d+1) p 

divisors :: Integral a => a -> [a]
divisors n = [x | x <- [1..(n - 1)], rem n x == 0]
