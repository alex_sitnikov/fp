-- �������� (��. ��������)
-- ���������� (/2)
module HW3
       ( root
       , sequence2_3_f
       , symmetric
       , listDigits
       , isHeap
       ) where
import Prelude
import Control.Applicative
import Data.Set (fromList, Set)
import Data.List
import Data.Char
       
-- |������� root ������ ��������� ������������ �������� ����� ���������
-- tan x = 1 - x � ���������, �������� ������ (� ������������) ���������� �������

f :: Double -> Double
f x = 1 - tan x

root :: Double -> Double
root eps = iterCalc eps 1e0 f

--�������, ������� ������������ ����� ������������ �������� ������ �����������
iterCalc :: Double -> Double -> (Double -> Double) -> Double
iterCalc eps x fun = let y = fun x
                     in if abs(y - x) < eps then y
                                            else iterCalc eps y fun

-- |����������� ������������� ������������������ ����� ����� ��� ����������
-- ���������� �� ���� ���������, ����� � ����������� ����������� �����
-- ([1, 2, 4, 8, 9, 16, 24, 25, 27, 36, 49, 64, ..]). �������� ��������� ���
-- ���������� n-�� ����� ���� ������������������.
sequence2_3_f :: Integer -> Integer
sequence2_3_f n = error "sequence2_3_f is not implemented"

-- |�������� ������� symmetric ��� �������� �������������� ��������� ������.
-- �������� ������ �������� ������������, ����, ������� ������������ �����
-- ����� �������� ����, ������ ��������� ����� �������� ���������� ����������
-- ������ ���������. ������������ ���� ��������� ��������� ������. �������� �
-- ���� ���������� �� ����.

data Tree a = Empty | Node (Tree a) a (Tree a)

symmetric :: Tree a -> Bool
symmetric Empty          = True
symmetric (Node a1 a a2) = eqRoots a1 a2 

foldTree  ::  (a -> b -> b) -> b -> Tree a -> b
foldTree _ seed Empty           =  seed
foldTree f seed (Node t1 n t2)  =  foldTree f (f n (foldTree f seed t2)) t1

flatten ::  Tree a -> [a]
flatten t  =  foldTree (:) [] t

eqRoots :: Tree a -> Tree b -> Bool
eqRoots a b = case (a, b) of
                  (Empty, Empty)                     -> True
                  (Node t11 n1 t12, Node t21 n2 t22) -> (eqRoots t11 t21) && (eqRoots t12 t22)
                  (_, _)                             -> False
                
-- |�������� ������� listDigits, ������� ��� ��������� ������ �� ����� ������
-- ������ ���� �����, ���������� ���� �� ���� �����.

data MultiTree a = Branch a [MultiTree a]

listDigits :: Tree String -> [String]
listDigits tree = filter containsDigit (flatten tree)

--���������� ������, ���� ���� �� ���� ����� ������� ����� ������� � �������� ������
containsDigit :: String -> Bool
containsDigit s = let ds = map intToDigit [0..9]
                  in or $ (map (\c -> containsChar c s)) ds
                  
containsChar :: Char -> String -> Bool
containsChar = any . (==)

-- |������� isHeap ��������� �������� �� ������ ���������, �� ���� ��������
-- � ������ �� ��� ����� ������ ��������, ���������� � ����������� � ����� ����.

isHeap :: Ord a => MultiTree a -> Bool
isHeap (Branch b [])  = True
isHeap (Branch b sub) = if y then and (map isHeap sub) else False
                            where y = filterMTree (b<) True sub

filterMTree :: (a -> Bool) -> Bool -> [MultiTree a] -> Bool
filterMTree p bol [] = bol
filterMTree p bol ((Branch b sub):bs) = filterMTree p ((&&) bol $ p b) bs