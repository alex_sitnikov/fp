module HW2
       ( list2_1
       , gcd1
       , coPrime
       , gcfList
       , perfectList
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Написать программу для нахождения минимального из
-- чисел, являющихся максимальными в каждой из строк заданной
-- прямоугольной матрицы.

list2_1 :: [[Integer]] -> Integer
list2_1 a = error "hw1_1 is not implemented"

-- |Вычислить наибольший общий делитель(НОД) 2х натуральных чисел. Использовать алгоритм Евклида.
gcd1 :: Integer -> Integer -> Integer
gcd1 a b = error "gcd1 is not implemented"

-- |Проверить являются ли 2 натуральных числа взаимнопростыми.
-- 2 числа называются взаимнопростыми если их НОД равен 1
coPrime :: Integer -> Integer -> Bool
coPrime a b = error "coPrime is not implemented"

-- |Напишите функцию нахождения самого маленького числа, которое делится
-- на все числа от одного до N.
gcfList :: [Integer] -> Integer
gcfList a = error "gcfList is not implemented"

-- |Написать программу для нахождения первых N совершенных чисел.
-- Совершенным числом называется натуральное число, равное
-- сумме всех своих делителей, включая единицу, но исключая само
-- это число. Так, например, число 28 – совершенное, поскольку
-- 28 = 1 + 2 + 4 + 7 + 14. 
perfectList :: Integer -> [Integer]
perfectList a = error "perfectList is not implemented"
