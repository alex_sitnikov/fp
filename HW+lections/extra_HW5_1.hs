module HW5_1
   ( rotateRight
   , sortList
   , multiPrimeFactor
   , sphenicList
   , calc
   ) where
    
-- |������� rotateRight ��������� ������ ������������ ������ � ������������� ����� n
-- ����� ���� ���������� ����������� �������� ������� n ���. [a, b, c, d, e] -> [e, a, b, c, d] 
--						n
rotateRight :: [a] -> Int -> [a]
rotateRight = error "rotateRight is not implemented"

-- |������� sortList ��������� ��������� ������ �� ����� ��� ����������.
sortList :: [[a]] -> [[a]]
sortList = error "sortList is not implemented"

-- |������� multiPrimeFactor �� ��������� ����� ����� ������ �� ��� ������� ���������
-- � ���������� �� ���������. ������ ����������� � ������� ����������� ������� ���������.
--							prime,count
multiPrimeFactor :: Int -> [(Int,Int)]
multiPrimeFactor = error "multiPrimeFactor is not implemented"

-- |������� sphenicList ������������ ������ ����������� ����� � ���������� �� � �� �.
-- ru.wikipedia.org/wiki/�����������_�����
--				�		�
sphenicList :: Int -> Int -> [a]
sphenicList = error "sphenicList is not implemented"

-- |�������  ������������ �������� ������������� ��������� ����������� � ���������� �����.
-- ������, ["one","two","one","+","six","*","two","six"] -> (121 + 6) * 26 = 3302
-- ������������ ���������: +, -, *, /, % (������� �� ������) , ^ (������� �����)
-- ���������� �������� ���, ������ ��� ���������� ������������ ����� ������� ���������������.
calc :: [String] -> Int
calc = error "calc is not implemented"