module HW3
       ( root
       , sequence2_3_f
       , symmetric
       , listDigits
       , isHeap
       ) where
-- |������� root ������ ��������� ������������ �������� ����� ���������
-- tan x = 1 - x � ���������, �������� ������ (� ������������) ����������
-- �������
f :: Double -> Double
f x = tan x + x - 1

root :: Double -> Double
root eps = error "root is not implemented"

-- |����������� ������������� ������������������ ����� ����� ��� ����������
-- ���������� �� ���� ���������, ����� � ����������� ����������� �����
-- ([1, 2, 4, 8, 9, 16, 24, 25, 27, 36, 49, 64, ..]). �������� ��������� ���
-- ���������� n-�� ����� ���� ������������������.
sequence2_3_f :: Integer -> Integer
sequence2_3_f = error "sequence2_3_f is not implemented"

-- |�������� ������� symmetric ��� �������� �������������� ��������� ������.
-- �������� ������ �������� ������������, ����, ������� ������������ �����
-- ����� �������� ����, ������ ��������� ����� �������� ���������� ����������
-- ������ ���������. ������������ ���� ��������� ��������� ������. �������� �
-- ���� ���������� �� ����.
data Tree a = Empty | Node (Tree a) a (Tree a)

symmetric ::  Tree a -> Bool
symmetric = error "symmetric is not implemented"

-- |�������� ������� listDigits, ������� ��� ��������� ������ �� ����� ������
-- ������ ���� �����, ���������� ���� �� ���� �����.

data MultiTree a = Branch a [MultiTree a]

listDigits :: MultiTree String -> [String]
listDigits = error "listDigits is not implemented"

-- |������� isHeap ��������� �������� �� ������ ���������, �� ���� ��������
-- � ������ �� ��� ����� ������ ��������, ���������� � ����������� � ����� ����.

isHeap :: Ord a => MultiTree a -> Bool
isHeap = error "isHeap is not implemented"