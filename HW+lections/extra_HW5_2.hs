module HW5_1
   ( bottom_up
   , internalPathLen
   , сonnectedСomponent
   , maxPath
   , bipartiteGraph
   ) where
    

-- |Написать функцию bottom_up, которая построит список узлов дерева от листьев к корню.
data Tree a = Node a [Tree a]

bottom_up  :: Tree a -> [a]
bottom_up  = error "bottom_up  is not implemented"

-- |Написать функцию internalPathLen, которая подсчитает сумму всех путей дерева от корня к листьям.
internalPathLen :: Tree a -> Int
internalPathLen = error "internalPathLen is not implemented"

-- |Написать функцию сonnectedСomponent, которая проверяет не содержит ли граф больше N компонент связанности.
type Graph = [(Int, [Int])]
--								N
сonnectedСomponent :: Graph -> Int -> Bool
сonnectedСomponent = error "сonnectedСomponent is not implemented"

-- |Написать функцию maxPath, которая выдает список вершин с наибольшей длиной маршрута между двумя заданными
-- вершинами. Если максимальных маршрутов несколько, то вывести все. Если маршрута между вершинами не существует,
-- то функция должна выдавать пустой список.
maxPath :: Graph -> Int -> Int -> [[a]]
maxPath = error "maxPath is not implemented"


-- |Написать функцию bipartiteGraph, которая проверяет, является ли граф двудольным.
bipartiteGraph :: Graph -> Bool
bipartiteGraph = error "bipartiteGraph is not implemented"
